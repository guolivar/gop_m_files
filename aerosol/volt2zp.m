function Zp=volt2zp(dmaV,L,Rin,Rout,flowsh)
#Calculate the equivalent electrical mobility for a given DMA voltage
# Input
#   dmaV      Voltage in DMA            VOLTS
#   dmaN      Number concentration RAW  pt/cc
#   L         DMA length                cm
#   Rin       DMA internal radius       cm
#   Rout      DMA external radius       cm
#   flowsh    Sheath Flow               l/min
# Output
#   Zp        Electrical mobility selected for the
#		given column voltage			m^2/()V*S)

#Convert flow to m3/s
flowsh=flowsh/(1000*60);
#Ratio of column radii
lnRoutRin=log(Rout/Rin);
#column length to metres
L=L/100;
Zp=abs(flowsh*lnRoutRin/(L*dmaV*2*pi));
