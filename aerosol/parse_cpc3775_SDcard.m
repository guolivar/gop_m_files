function parse_cpc3775_SDcard(work_dir)
page_screen_output(0);
page_output_immediately(1);
%% Which file to work with
#work_dir='/home/olivaresga/data/DIDACTIC/Queen_Street/data_3775/from_memory_card/';
id_out=fopen([work_dir 'data_out.data'],'wt');
id_out_err=fopen([work_dir 'error_out.data'],'wt');
files=dir([work_dir '*.dat']);
files_dates=[files(:).datenum]';
[sorted_date indx]=sort(files_dates);
id_n4=408;
id_error=409;
id_site=2;
fprintf(id_out,'%s\n','year,month,day,hour,minute,second,parameterid,value,siteid,recordtime');
for j=1:length(indx)
  i=indx(j);
  # Only process files that are at least 250bytes in size
  # (a few rows of data at least)
  if files(i).bytes>250
    files(i).name
    id_in=fopen([work_dir files(i).name],'rt');
    # Ignore first line
    cline=fgetl(id_in);
    # Second line contains start date information
    cline=fgetl(id_in);
    start_date=cline(13:end);
    # Third line contains interval in seconds
    cline=fgetl(id_in);
    interval=sscanf(cline,'%d');
    day_scale=interval/(24*60*60)
    # Fourth line contains serial number
    cline=fgetl(id_in);
    serialn=['AC-' cline(end-10:end)];
    #Fifth line onwards has actual measurements
    ii=0;
    while ~feof(id_in)
      cline=fgetl(id_in);
      data_vec=sscanf(cline,'%d,%e,%f,%f,%d');
      conc=data_vec(2);
      errors=data_vec(5);
      c_date=datevec(datenum(start_date,'ddd mmm dd HH:MM:SS YYYY')+ii*day_scale);
      ii=ii+1;
      # output lines
      # DATA
      fprintf(id_out,'%d,%d,%d,%d,%d,%d,',c_date);
      fprintf(id_out,'%d,%d,%d,',id_n4,conc,id_site);
      fprintf(id_out,'%s\n',datestr(c_date,"'yyyy-mm-dd HH:MM:SS'"));
      # ERRORS      
      fprintf(id_out_err,'%d,%d,%d,%d,%d,%d,',c_date);
      fprintf(id_out_err,'%d,%d,%d,',id_error,errors,id_site);
      fprintf(id_out_err,'%s\n',datestr(c_date,"'yyyy-mm-dd HH:MM:SS.FFF'"));
    end
    fclose(id_in);
  end
end
fclose(id_out);
fclose(id_out_err);
