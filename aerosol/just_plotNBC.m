
date1=datenum([2012 3 3 0 0 0]);
date2=datenum([2012 3 3 23 0 0]);
period=datestr((date1+date2)/2,'yyyy-mm-dd');
# site=['BC_SOAP' period];
# #Indices to plot
# plot_vec=find(and(ae21.data(:,1)>=date1,ae21.data(:,1)<=date2));
# plot(ae21.data(plot_vec,1),ae21.smooth_data(plot_vec,1),'-k',ae21.data(plot_vec,1),ae21.smooth_data(plot_vec,2),'-r');
# legend('BC','uvBC');
# datetick('x','mm/dd HH:MM');
# ylabel('[ng/m^3]');
# xlabel('Local Time');
# saveas(gcf(),[site '_LUKE_DATE.png']);
# close(gcf());

site=['N_SOAP' period];
#Indices to plot
plot_vec=find(and(cpc.cpc_data(:,1)>=date1,cpc.cpc_data(:,1)<=date2));
plot(cpc.cpc_data(plot_vec,1),cpc.cpc_data(plot_vec,7),'-k');
ylim([0 10000]);
legend('N_{10}');
datetick('x','mm/dd HH:MM');
ylabel('[#/cm^3]');
xlabel('Local Time');
# saveas(gcf(),[site '_LUKE_DATA.png']);
