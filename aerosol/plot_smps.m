#fclose all;
#close all
#clear
#It plots the end result as a shaded timeseries
#The first line contains the headers 7 for the date and date_vector and then the centre sizes

#Set the scene
work_dir='/run/media/gustavo/Win7Portable/data/smps/otahuhu/SMPS_000/';
work_file='transposed_inv.txt';
#moving average window ... in SCANS!!!
wndw=10;
#parse the bin central sizes
bin_sizes=dlmread([work_dir 'sizes.txt'],'\t');
nsizes=length(bin_sizes);
id_in=fopen([work_dir work_file],'rt');
header=fgetl(id_in);
fclose(id_in);
#parse the data file
data_in=dlmread([work_dir work_file],'\t',1,0);
#only deal with the size distribution part of the data
dndlogdp=data_in(:,8:8+nsizes-1);
nrecords=length(data_in);
date_vector=data_in(:,1);
clear data_in;
smooth_data=dndlogdp;
smooth_data(smooth_data<=0)=NaN;
smooth_data_aux=smooth_data;
%Smooth the data according to the WNDW
for i=wndw+1:nrecords
	smooth_data(i,:)=nanmedian(smooth_data_aux(i-wndw:i,:));
end
#Save hdf5 file
save('-hdf5',[work_dir work_file '.h5'],'header','dndlogdp','bin_sizes','date_vector','smooth_data');
#one plot per day
nrecDAY=480;
startday=2;
for i=startday:(nrecDAY/10):(floor((nrecords-startday)/nrecDAY)-1)*nrecDAY
  time_window=(i:nrecDAY+i);
  #plot the shaded time series LINEAR SCALING
  figure(1);
  pcolor(date_vector(time_window),bin_sizes,smooth_data(time_window,:)');
  shading('interp');
  caxis([0 50000]);
  set(gca,'yscale','log');
  axis tight;
  datetick('x',15,'keeplimits');
  colorbar;
  title(datestr(median(date_vector(time_window)),1));
#   saveas(gcf,[work_dir 'linear_' datestr(median(date_vector(time_window)),'%Y%m%d%H') '.png'],'png');
	  saveas(gcf,[work_dir 'linear_' num2str(i,'%06.0f') '.png'],'png');
  close(1);
  #plot the shaded time series LOGSCALING
  figure(1);
  pcolor(date_vector(time_window),bin_sizes,log10(smooth_data(time_window,:))');
  shading('interp');
  caxis([1 6]);
  set(gca,'yscale','log');
  axis tight;
  datetick('x',15,'keeplimits');
  colorbar;
  title(datestr(median(date_vector(time_window)),1))
#   saveas(gcf,[work_dir 'log_' datestr(median(date_vector(time_window)),'%Y%m%d%H') '.png'],'png');
  saveas(gcf,[work_dir 'log_' num2str(i,'%06.0f') '.png'],'png');
  close(1)
end
time_window=(floor((nrecords-startday)/nrecDAY)*nrecDAY:nrecords);
#plot the shaded time series
figure(1);
pcolor(date_vector(time_window),bin_sizes,smooth_data(time_window,:)');
shading('interp');
caxis([0 50000]);
set(gca,'yscale','log');
axis tight;
datetick('x',15,'keeplimits');
colorbar;
title(datestr(median(date_vector(time_window)),1))
# saveas(gcf,[work_dir 'linear_' datestr(median(date_vector(time_window)),'%Y%m%d%H') '.png'],'png');
saveas(gcf,[work_dir 'linear_' num2str(i+1,'%06.0f') '.png'],'png');
close(1);
#plot the shaded time series
figure(1);
pcolor(date_vector(time_window),bin_sizes,log10(smooth_data(time_window,:))');
shading('interp');
caxis([1 6]);
set(gca,'yscale','log');
axis tight;
datetick('x',15,'keeplimits');
colorbar;
title(datestr(median(date_vector(time_window)),1))
# saveas(gcf,[work_dir 'log_' datestr(median(date_vector(time_window)),'%Y%m%d%H') '.png'],'png');
saveas(gcf,[work_dir 'log_' num2str(i+1,'%06.0f') '.png'],'png');
close all