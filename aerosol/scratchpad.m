avg_data=NaN*zeros(3668,39);
j=1;
for i=6:5:31690
  avg_data(j,:)=nanmedian(grimm_data(i-5:i,:));
  j=j+1;
end
close(1);
figure(1);
pcolor(avg_data(:,1),grimm_dp,log10(avg_data(:,9:end))');
shading('interp');
set(gca,'yscale','log');
axis('tight');
# ylim([265 1000]);
datetick('keeplimits','dd-mmm');
saveas(1,'/home/gustavo/Documents/Presentations/SOAP_workshop2013/grimm.png');