work_dir='/home/gustavo/data/soap/smps_data/';
work_file='transposed_inv.txt';
smps_data=dlmread([work_dir work_file],'\t',1,0);
nrecs=length(smps_data(:,1));
Bubble_flag=ones(size(smps_data(:,1)));
nbins=30;
dmin=8;
dmax=300;
scaling=(dmax/dmin)^(1/(nbins-1));
sizes=ones(nbins,1);
sizes(1)=dmin;
fmt_out='%f\t%f\t%f\t%f\t%f\t%f\t%f\t';
fmt_dndlog='%f';
for i=2:nbins
  sizes(i)=sizes(i-1)*scaling;
  fmt_dndlog=[fmt_dndlog '\t%f'];
end
sizes_low=sizes/sqrt(scaling);
sizes_high=sizes*sqrt(scaling);
Ndma=NaN*zeros(nrecs,1);
for i=1:nrecs
  Ndma(i)=0.054277*nansum(smps_data(i,2:2+nbins-1));
end
plot(smps_data(:,1),Ndma);