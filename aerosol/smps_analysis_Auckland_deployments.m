fclose all;
close all
clear
#It plots the end result as a shaded timeseries
#The first line contains the headers 7 for the date and date_vector and then the centre sizes

#Load data Otahuhu
work_dir='/run/media/gustavo/gus_external/work/data/smps/otahuhu/SMPS_000/';
work_file='transposed_inv.txt.h5';
data_Otahuhu=load([work_dir work_file]);
data_Otahuhu.date_expanded=datevec(data_Otahuhu.date_vector);

#Load data Mangere
work_dir='/run/media/gustavo/gus_external/work/data/smps/Mangere_res/FixedData/15min_ave/';
work_file='smooth_pasted.txt.h5';
data_Mangere=load([work_dir work_file]);
data_Mangere.date_expanded=datevec(data_Mangere.date_vector);

#Load data Mt Eden
work_dir='/run/media/gustavo/gus_external/work/data/smps/MtEden/';
work_file='smooth_20120918.txt.h5';
data_MtEden=load([work_dir work_file]);
data_MtEden.date_expanded=datevec(data_MtEden.date_vector);

#Load data Newmarket
work_dir='/run/media/gustavo/gus_external/work/data/smps/Newmarket/Summaires_and_plots/';
work_file='smooth_20120330.txt.h5';
data_Newmarket=load([work_dir work_file]);
data_Newmarket.date_expanded=datevec(data_Newmarket.date_vector);


#data structure
#data.bin_sizes
#data.date_vector
#data.dndlogdp
#data.smooth_data

#Select the right time to summarize
#Hours
hmin=7;
hmax=19;
leg1='07:00 - 19:00';
leg2='19:00 - 07:00';
h1=17;
h2=19;
site='Auckland';
period='2010 - 2012';

diurnal_summary_Otahuhu=nanmedian(data_Otahuhu.dndlogdp(and(data_Otahuhu.date_expanded(:,4)>=hmin,data_Otahuhu.date_expanded(:,4)<=hmax),:));
diurnal_summary_Mangere=nanmedian(data_Mangere.dndlogdp(and(data_Mangere.date_expanded(:,4)>=hmin,data_Mangere.date_expanded(:,4)<=hmax),:));
diurnal_summary_MtEden=nanmedian(data_MtEden.dndlogdp(and(data_MtEden.date_expanded(:,4)>=hmin,data_MtEden.date_expanded(:,4)<=hmax),:));
diurnal_summary_Newmarket=nanmedian(data_Newmarket.dndlogdp(and(data_Newmarket.date_expanded(:,4)>=hmin,data_Newmarket.date_expanded(:,4)<=hmax),:));

diurnal_summary_Otahuhu_N=diurnal_summary_Otahuhu/sum(diurnal_summary_Otahuhu*log10(data_Otahuhu.bin_sizes(2)/data_Otahuhu.bin_sizes(1)));
diurnal_summary_Mangere_N=diurnal_summary_Mangere/sum(diurnal_summary_Mangere*log10(data_Mangere.bin_sizes(2)/data_Mangere.bin_sizes(1)));
diurnal_summary_MtEden_N=diurnal_summary_MtEden/sum(diurnal_summary_MtEden*log10(data_MtEden.bin_sizes(2)/data_MtEden.bin_sizes(1)));
diurnal_summary_Newmarket_N=diurnal_summary_Newmarket/sum(diurnal_summary_Newmarket*log10(data_Newmarket.bin_sizes(2)/data_Newmarket.bin_sizes(1)));


nocturnal_summary_Otahuhu=nanmedian(data_Otahuhu.dndlogdp(or(data_Otahuhu.date_expanded(:,4)<=hmin,data_Otahuhu.date_expanded(:,4)>=hmax),:));
nocturnal_summary_Mangere=nanmedian(data_Mangere.dndlogdp(or(data_Mangere.date_expanded(:,4)<=hmin,data_Mangere.date_expanded(:,4)>=hmax),:));
nocturnal_summary_MtEden=nanmedian(data_MtEden.dndlogdp(or(data_MtEden.date_expanded(:,4)<=hmin,data_MtEden.date_expanded(:,4)>=hmax),:));
nocturnal_summary_Newmarket=nanmedian(data_Newmarket.dndlogdp(or(data_Newmarket.date_expanded(:,4)<=hmin,data_Newmarket.date_expanded(:,4)>=hmax),:));

nocturnal_summary_Otahuhu_N=nocturnal_summary_Otahuhu/sum(nocturnal_summary_Otahuhu*log10(data_Otahuhu.bin_sizes(2)/data_Otahuhu.bin_sizes(1)));
nocturnal_summary_Mangere_N=nocturnal_summary_Mangere/sum(nocturnal_summary_Mangere*log10(data_Mangere.bin_sizes(2)/data_Mangere.bin_sizes(1)));
nocturnal_summary_MtEden_N=nocturnal_summary_MtEden/sum(nocturnal_summary_MtEden*log10(data_MtEden.bin_sizes(2)/data_MtEden.bin_sizes(1)));
nocturnal_summary_Newmarket_N=nocturnal_summary_Newmarket/sum(nocturnal_summary_Newmarket*log10(data_Newmarket.bin_sizes(2)/data_Newmarket.bin_sizes(1)));


# Raw distributions
# Daytime
loglog(data_Otahuhu.bin_sizes,diurnal_summary_Otahuhu,'-r' ...
	,data_Mangere.bin_sizes,diurnal_summary_Mangere,'-k' ...
	,data_MtEden.bin_sizes,diurnal_summary_MtEden,'-b' ...
	,data_Newmarket.bin_sizes,diurnal_summary_Newmarket,'-g'	);
xlim([10 1000]);
title([site ' 07:00 - 19:00']);
xlabel('D_P [nm]');
ylabel('dN/dLogD_P [#/cc]');
legend('Otahuhu','Mangere','Mt Eden','Newmarket');
saveas(gcf(),[site '_daytime_summary.png']);
close(gcf());

semilogx(data_Otahuhu.bin_sizes,diurnal_summary_Otahuhu,'-r' ...
	,data_Mangere.bin_sizes,diurnal_summary_Mangere,'-k' ...
	,data_MtEden.bin_sizes,diurnal_summary_MtEden,'-b' ...
	,data_Newmarket.bin_sizes,diurnal_summary_Newmarket,'-g'	);
xlim([10 1000]);
title([site ' 07:00 - 19:00']);
xlabel('D_P [nm]');
ylabel('dN/dLogD_P [#/cc]');
legend('Otahuhu','Mangere','Mt Eden','Newmarket');
saveas(gcf(),[site '_daytime_summary_LIN.png']);
close(gcf());
# Nighttime
loglog(data_Otahuhu.bin_sizes,nocturnal_summary_Otahuhu,'-r' ...
	,data_Mangere.bin_sizes,nocturnal_summary_Mangere,'-k' ...
	,data_MtEden.bin_sizes,nocturnal_summary_MtEden,'-b' ...
	,data_Newmarket.bin_sizes,nocturnal_summary_Newmarket,'-g'	);
xlim([10 1000]);
title([site ' 19:00 - 07:00']);
xlabel('D_P [nm]');
ylabel('dN/dLogD_P [#/cc]');
legend('Otahuhu','Mangere','Mt Eden','Newmarket');
saveas(gcf(),[site '_night_summary.png']);
close(gcf());

semilogx(data_Otahuhu.bin_sizes,nocturnal_summary_Otahuhu,'-r' ...
	,data_Mangere.bin_sizes,nocturnal_summary_Mangere,'-k' ...
	,data_MtEden.bin_sizes,nocturnal_summary_MtEden,'-b' ...
	,data_Newmarket.bin_sizes,nocturnal_summary_Newmarket,'-g'	);
xlim([10 1000]);
title([site ' 19:00 - 07:00']);
xlabel('D_P [nm]');
ylabel('dN/dLogD_P [#/cc]');
legend('Otahuhu','Mangere','Mt Eden','Newmarket');
saveas(gcf(),[site '_night_summary_LIN.png']);
close(gcf());




# NORMALIZED distributions
# Daytime
loglog(data_Otahuhu.bin_sizes,diurnal_summary_Otahuhu_N,'-r' ...
	,data_Mangere.bin_sizes,diurnal_summary_Mangere_N,'-k' ...
	,data_MtEden.bin_sizes,diurnal_summary_MtEden_N,'-b' ...
	,data_Newmarket.bin_sizes,diurnal_summary_Newmarket_N,'-g'	);
xlim([10 1000]);
title([site ' 07:00 - 19:00']);
xlabel('D_P [nm]');
ylabel('dN/dLogD_P [#/cc]');
legend('Otahuhu','Mangere','Mt Eden','Newmarket');
saveas(gcf(),[site '_daytime_summary_N.png']);
close(gcf());

semilogx(data_Otahuhu.bin_sizes,diurnal_summary_Otahuhu_N,'-r' ...
	,data_Mangere.bin_sizes,diurnal_summary_Mangere_N,'-k' ...
	,data_MtEden.bin_sizes,diurnal_summary_MtEden_N,'-b' ...
	,data_Newmarket.bin_sizes,diurnal_summary_Newmarket_N,'-g'	);
xlim([10 1000]);
title([site ' 07:00 - 19:00']);
xlabel('D_P [nm]');
ylabel('dN/dLogD_P [#/cc]');
legend('Otahuhu','Mangere','Mt Eden','Newmarket');
saveas(gcf(),[site '_daytime_summary_LIN_N.png']);
close(gcf());
# Nighttime
loglog(data_Otahuhu.bin_sizes,nocturnal_summary_Otahuhu_N,'-r' ...
	,data_Mangere.bin_sizes,nocturnal_summary_Mangere_N,'-k' ...
	,data_MtEden.bin_sizes,nocturnal_summary_MtEden_N,'-b' ...
	,data_Newmarket.bin_sizes,nocturnal_summary_Newmarket_N,'-g'	);
xlim([10 1000]);
title([site ' 19:00 - 07:00']);
xlabel('D_P [nm]');
ylabel('dN/dLogD_P [#/cc]');
legend('Otahuhu','Mangere','Mt Eden','Newmarket');
saveas(gcf(),[site '_night_summary_N.png']);
close(gcf());

semilogx(data_Otahuhu.bin_sizes,nocturnal_summary_Otahuhu_N,'-r' ...
	,data_Mangere.bin_sizes,nocturnal_summary_Mangere_N,'-k' ...
	,data_MtEden.bin_sizes,nocturnal_summary_MtEden_N,'-b' ...
	,data_Newmarket.bin_sizes,nocturnal_summary_Newmarket_N,'-g'	);
xlim([10 1000]);
title([site ' 19:00 - 07:00']);
xlabel('D_P [nm]');
ylabel('dN/dLogD_P [#/cc]');
legend('Otahuhu','Mangere','Mt Eden','Newmarket');
saveas(gcf(),[site '_night_summary_LIN_N.png']);
close(gcf());
