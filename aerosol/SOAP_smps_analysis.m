#fclose all;
#close all
#clear
#It plots the end result as a shaded timeseries
#The first line contains the headers 7 for the date and date_vector and then the centre sizes

#Set the scene
work_dir='/home/olivaresga/DATA/SOAP/smps_data/';
work_file='transposed_inv_QA.txt';
#load data
id_in=fopen([work_dir work_file],'rt');
headerlin=fgetl(id_in);
fclose(id_in);
dataIN=dlmread([work_dir work_file],'\t',1,0);
data=struct('date_vector',[],'date_expanded',[],'dndlogdp',[],'bin_sizes',[],'smooth_data',[]);
data.date_vector=dataIN(:,1);
data.date_expanded=datevec(data.date_vector);
data.dndlogdp=dataIN(:,10:end);
data.bin_sizes=sscanf(headerlin(64:end),'%f');
#moving average window ... in SCANS!!!
wndw=10;
data.smooth_data=data.dndlogdp;
for i=wndw+1:length(data.dndlogdp(:,1))
	data.smooth_data(i,:)=nanmedian(data.dndlogdp(i-wndw:i,:));
end
%GRIMM
work_dir='/home/olivaresga/DATA/SOAP/';
work_file='grimm_data.h5';
grimm=load([work_dir work_file]);
grimm.dndlogdp=grimm.grimm_data(:,8:end);
grimm.grimm_data(:,1)=datenum(datevec(grimm.grimm_data(:,1))+[2000 0 0 0 0 0]);
#Select the right time to summarize
#Hours
hmin=7;
hmax=19;
leg1='07:00 - 19:00';
leg2='19:00 - 07:00';
h1=17;
h2=19;
date1=datenum([2012 3 1 6 0 0]);
date2=datenum([2012 3 3 12 0 0]);
period=datestr((date1+date2)/2,'yyyy-mm-dd');
site=['SOAP-' period];
#Indices to plot
plot_vec=find(and(data.date_vector>=date1,data.date_vector<=date2));
grimm_vec=find(and(grimm.grimm_data(:,1)>=date1,grimm.grimm_data(:,1)<=date2));
%~ for i=1:length(plot_vec)
	%~ cycle_summary(i,:)=data.smooth_data(plot_vec(i),:);
	%~ cycle_summary_norm(i,:)=data.smooth_data(plot_vec(i),:)/(nansum(data.smooth_data(plot_vec(i),:)));
%~ end
smps.work_data=data.dndlogdp(plot_vec,:);
smps.work_date=data.date_expanded(plot_vec,:);
smps.diurnal_summary=nanmedian(smps.work_data);
grimm.work_data=grimm.dndlogdp(grimm_vec,:);
grimm.diurnal_summary=nanmedian(grimm.work_data);
figure(1);
loglog(data.bin_sizes,smps.diurnal_summary,'-r',grimm.grimm_dp,grimm.diurnal_summary,'-k');
title([site]);
xlabel('D_P [nm]');
ylabel('dN/dLogD_P [#/cc]');
saveas(gcf(),[site '_LukeDATE.png']);

figure(2);
sample_indx=plot_vec;
pcolor(smps_data(sample_indx,1),data.bin_sizes',log10(data.dndlogdp(sample_indx,:))');
shading('interp');
set(gca,'yscale','log');
datetick('x','keeplimits');
colorbar;
saveas(gcf(),[site '_LukeDATE_SHADED.png']);

%~ 
%~ cycle_summary=NaN*zeros(24,length(diurnal_summary));
%~ cycle_summary_norm=cycle_summary;
%~ for i=1:23
	%~ cycle_summary(i,:)=nanmedian(work_data(work_date(:,4)==i,:));
	%~ cycle_summary_norm(i,:)=cycle_summary(i,:)/(nansum(cycle_summary(i,:))/log10(data.bin_sizes(2)/data.bin_sizes(1)));
%~ end
%~ cycle_summary(24,:)=nanmedian(work_data(work_date(:,4)==0,:));
%~ cycle_summary_norm(24,:)=cycle_summary(24,:)/(nansum(cycle_summary(24,:))/log10(data.bin_sizes(2)/data.bin_sizes(1)));
%~ 
%~ 
%~ 
%~ 
%~ figure(1);
%~ pcolor([1:24]/24,data.bin_sizes,cycle_summary');
%~ shading('interp');
%~ title([site]);
%~ %caxis([0 50000]);
%~ set(gca,'yscale','log');
%~ axis tight;
%~ datetick('x',15,'keeplimits');
%~ colorbar;
%~ saveas(gcf(),[site '_summary_LIN_CYCLE.png']);
%~ close(gcf());
%~ figure(1);
%~ pcolor([1:24]/24,data.bin_sizes,cycle_summary_norm');
%~ shading('interp');
%~ title([site]);
%~ %caxis([0 50000]);
%~ set(gca,'yscale','log');
%~ axis tight;
%~ datetick('x',15,'keeplimits');
%~ colorbar;
%~ saveas(gcf(),[site '_summary_LINNORM_CYCLE.png']);
%~ close(gcf());



%~ diurnal_summary=nanmedian(data.dndlogdp(and(data.date_expanded(:,4)>=hmin,data.date_expanded(:,4)<=hmax),:));
%~ nocturnal_summary=nanmedian(data.dndlogdp(or(data.date_expanded(:,4)<=hmin,data.date_expanded(:,4)>=hmax),:));
#nocturnal_summary=nanmedian(data.dndlogdp(and(data.date_expanded(:,4)<=h2,data.date_expanded(:,4)>=h1),:));
%~ 
%~ loglog(data.bin_sizes,diurnal_summary,'-r',data.bin_sizes,nocturnal_summary,'-k');
%~ title([site ' ' period]);
%~ xlabel('D_P [nm]');
%~ ylabel('dN/dLogD_P [#/cc]');
%~ legend(leg1,leg2);
%~ saveas(gcf(),[site '_summary.png']);
%~ close(gcf());
%~ semilogx(data.bin_sizes,diurnal_summary,'-r',data.bin_sizes,nocturnal_summary,'-k');
%~ title([site ' ' period]);
%~ xlabel('D_P [nm]');
%~ ylabel('dN/dLogD_P [#/cc]');
%~ legend(leg1,leg2);
%~ saveas(gcf(),[site '_summary_LIN.png']);
%~ 
%~ diurnal_summary=diurnal_summary/sum(diurnal_summary*log10(data.bin_sizes(2)/data.bin_sizes(1)));
%~ nocturnal_summary=nocturnal_summary/sum(nocturnal_summary*log10(data.bin_sizes(2)/data.bin_sizes(1)));
%~ loglog(data.bin_sizes,diurnal_summary,'-r',data.bin_sizes,nocturnal_summary,'-k');
%~ title([site ' ' period]);
%~ xlabel('D_P [nm]');
%~ ylabel('dN/dLogD_P [#/cc]');
%~ legend(leg1,leg2);
%~ saveas(gcf(),[site '_summaryNORM.png']);
%~ close(gcf());
%~ semilogx(data.bin_sizes,diurnal_summary,'-r',data.bin_sizes,nocturnal_summary,'-k');
%~ title([site ' ' period]);
%~ xlabel('D_P [nm]');
%~ ylabel('dN/dLogD_P [#/cc]');
%~ legend(leg1,leg2);
%~ saveas(gcf(),[site '_summary_LINNORM.png']);
%~ close(gcf());

%~ 



#data structure
#data.bin_sizes
#data.date_vector
#data.dndlogdp
#data.smooth_data

#Plot by and hour
%~ diurnal_summary=nanmedian(data.dndlogdp);
%~ cycle_summary=zeros(24,length(diurnal_summary));
%~ cycle_summary_norm=cycle_summary;
%~ for i=1:23
	%~ cycle_summary(i,:)=nanmedian(data.dndlogdp(data.date_expanded(:,4)==i,:));
	%~ cycle_summary_norm(i,:)=cycle_summary(i,:)/(sum(cycle_summary(i,:))/log10(data.bin_sizes(2)/data.bin_sizes(1)));
%~ end
%~ cycle_summary(24,:)=nanmedian(data.dndlogdp(data.date_expanded(:,4)==0,:));
%~ cycle_summary_norm(24,:)=cycle_summary(24,:)/(sum(cycle_summary(24,:))/log10(data.bin_sizes(2)/data.bin_sizes(1)));
%~ 
%~ figure(1);
%~ pcolor([1:24]/24,data.bin_sizes,cycle_summary');
%~ shading('interp');
%~ %caxis([0 50000]);
%~ set(gca,'yscale','log');
%~ axis tight;
%~ datetick('x',15,'keeplimits');
%~ colorbar;
%~ saveas(gcf(),[site '_summary_LIN_CYCLE.png']);
%~ close(gcf());
%~ figure(1);
%~ pcolor([1:24]/24,data.bin_sizes,cycle_summary_norm');
%~ shading('interp');
%~ %caxis([0 50000]);
%~ set(gca,'yscale','log');
%~ axis tight;
%~ datetick('x',15,'keeplimits');
%~ colorbar;
%~ saveas(gcf(),[site '_summary_LINNORM_CYCLE.png']);
%~ close(gcf());
%~ 
%~ figure(1);
%~ pcolor([1:24]/24,data.bin_sizes,log10(cycle_summary'));
%~ shading('interp');
%~ %caxis([0 50000]);
%~ set(gca,'yscale','log');
%~ axis tight;
%~ datetick('x',15,'keeplimits');
%~ colorbar;
%~ saveas(gcf(),[site '_summary_LOG_CYCLE.png']);
%~ close(gcf());
%~ figure(1);
%~ pcolor([1:24]/24,data.bin_sizes,log10(cycle_summary_norm'));
%~ shading('interp');
%~ %caxis([0 50000]);
%~ set(gca,'yscale','log');
%~ axis tight;
%~ datetick('x',15,'keeplimits');
%~ colorbar;
%~ saveas(gcf(),[site '_summary_LOGNORM_CYCLE.png']);
%~ close(gcf());
