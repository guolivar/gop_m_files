fclose all;
close all
clear
#It plots the end result as a shaded timeseries
#The first line contains the headers 7 for the date and date_vector and then the centre sizes

#Set the scene
work_dir='/run/media/olivaresga/gus_external/work/data/smps/Mangere_res/FixedData/15min_ave/';
work_file='smooth_pasted.txt.h5';
#load data
data=load([work_dir work_file]);
data.date_expanded=datevec(data.date_vector);

#data structure
#data.bin_sizes
#data.date_vector
#data.dndlogdp
#data.smooth_data

#Select the right time to summarize
#Hours
hmin=7;
hmax=19;
leg1='07:00 - 19:00';
leg2='19:00 - 07:00';
h1=17;
h2=19;
site='Mangere';
period='Dec 2010';
diurnal_summary=nanmedian(data.dndlogdp(and(data.date_expanded(:,4)>=hmin,data.date_expanded(:,4)<=hmax),:));
nocturnal_summary=nanmedian(data.dndlogdp(or(data.date_expanded(:,4)<=hmin,data.date_expanded(:,4)>=hmax),:));
#nocturnal_summary=nanmedian(data.dndlogdp(and(data.date_expanded(:,4)<=h2,data.date_expanded(:,4)>=h1),:));

loglog(data.bin_sizes,diurnal_summary,'-r',data.bin_sizes,nocturnal_summary,'-k');
title([site ' ' period]);
xlabel('D_P [nm]');
ylabel('dN/dLogD_P [#/cc]');
legend(leg1,leg2);
saveas(gcf(),[site '_summary.png']);
close(gcf());
semilogx(data.bin_sizes,diurnal_summary,'-r',data.bin_sizes,nocturnal_summary,'-k');
title([site ' ' period]);
xlabel('D_P [nm]');
ylabel('dN/dLogD_P [#/cc]');
legend(leg1,leg2);
saveas(gcf(),[site '_summary_LIN.png']);

diurnal_summary=diurnal_summary/sum(diurnal_summary*log10(data.bin_sizes(2)/data.bin_sizes(1)));
nocturnal_summary=nocturnal_summary/sum(nocturnal_summary*log10(data.bin_sizes(2)/data.bin_sizes(1)));
loglog(data.bin_sizes,diurnal_summary,'-r',data.bin_sizes,nocturnal_summary,'-k');
title([site ' ' period]);
xlabel('D_P [nm]');
ylabel('dN/dLogD_P [#/cc]');
legend(leg1,leg2);
saveas(gcf(),[site '_summaryNORM.png']);
close(gcf());
semilogx(data.bin_sizes,diurnal_summary,'-r',data.bin_sizes,nocturnal_summary,'-k');
title([site ' ' period]);
xlabel('D_P [nm]');
ylabel('dN/dLogD_P [#/cc]');
legend(leg1,leg2);
saveas(gcf(),[site '_summary_LINNORM.png']);
close(gcf());





