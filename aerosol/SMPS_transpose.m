fclose all
clear
# close all
# Transpose an inverted SMPS file
# expected structure Dp-dNdlogDp
# Set max and min valid Dps [8 – 307]
# Divide the range into 50 bins for output
# Loop until run out of file
#   Get 120 records (up AND down scan)
#   Sort by Size
# NOT  Interpolate to the 50 output bins
#   BIN the scan by the mid bins of the output sizes and average/median/geomAVG the values
#   to the output bin
#   output the time (start of scan) + dndlogDp for the 50 bins
# Close everything
page_screen_output(0);
page_output_immediately(1);
# Set the scene
work_dir='/home/gustavo/data/soap/smps_data/';
work_file='inv_smps_QA.txt';
transposed_file=['transposed_inv.txt'];
# mean_file=['transposed_inv_QA_MEAN.txt'];
# median_file=['transposed_inv_QA_MEDIAN.txt'];
id_in=fopen([work_dir work_file],'rt');
id_ou=fopen([work_dir transposed_file],'w');
# id_ou2=fopen([work_dir mean_file],'w');
# id_ou3=fopen([work_dir median_file],'w');
#Size range [nm] and number of bins
nbins=30;
dmin=8;
dmax=300;
scaling=(dmax/dmin)^(1/(nbins-1));
sizes=ones(nbins,1);
sizes(1)=dmin;
fmt_out='%f\t%f\t%f\t%f\t%f\t%f\t%f\t';
fmt_dndlog='%f';
for i=2:nbins
  sizes(i)=sizes(i-1)*scaling;
  fmt_dndlog=[fmt_dndlog '\t%f'];
end
sizes_low=sizes/sqrt(scaling);
sizes_high=sizes*sqrt(scaling);
#Start parsing the file
#skip the header line
line=fgetl(id_in);
#write output header line
fprintf(id_ou,['OctaveDate\tYear\tMonth\tDay\tHour\tMinute\tSecond\t' fmt_dndlog '\t' fmt_dndlog '\t' fmt_dndlog '\n'],[sizes sizes sizes]);
# fprintf(id_ou2,['OctaveDate\tYear\tMonth\tDay\tHour\tMinute\tSecond\t' fmt_dndlog],sizes);
# fprintf(id_ou3,['OctaveDate\tYear\tMonth\tDay\tHour\tMinute\tSecond\t' fmt_dndlog],sizes);
#data matrix for records
nread=300;
mat_data=zeros(nread,33);
report=0;
indx2=report;
while ~feof(id_in)
  #Read records, checking each time that the end of file hasn't been reached
  for i=1:nread
    line=fgetl(id_in);
    if feof(id_in)
      break;
    end
    #Parse the line and populate the matrix
    lin_data=sscanf(line,'%f')';
    mat_data(i,:)=lin_data;
    indx2=indx2+1;
    if indx2>report
      datestr(mat_data(1))
      report=report+3600*12;
    end
  end
  if feof(id_in)
    break;
  end
  in_sizes=mat_data(:,30);
  in_dn_dlogDp=mat_data(:,31);
  in_dn_dlogDp(mat_data(:,31)<=0)=NaN;
  # Find the indices of the records that need to go to the respective bins
  in_bins=group_by_size(in_sizes, sizes_low, sizes_high,true);
  out_dn_dlogDp=NaN*ones(1,30);
  out_dn_dlogDp2=out_dn_dlogDp;
  out_dn_dlogDp3=out_dn_dlogDp;
  for i=1:nbins
    indx=find(in_bins==i);
    in_dn_dlogDp(in_dn_dlogDp<0)=NaN;
    if length(indx)>0
      in_dn1=in_dn_dlogDp(indx);
      v_indx=in_dn1(~isnan(in_dn1));
      out_dn_dlogDp2(i)=nanmean([v_indx;NaN;NaN]);
      out_dn_dlogDp(i)=nanmedian([v_indx;NaN;NaN]);
      out_dn_dlogDp3(i)=nanstd([v_indx;NaN;NaN]);
    else
      out_dn_dlogDp(i)=NaN;
    end
  end
#   out_dn_dlogDp(find(isna(out_dn_dlogDp)))=NaN;
#   figure(2);
#   plot(out_dn_dlogDp);
#   pause
  fprintf(id_ou,[fmt_out fmt_dndlog '\t' fmt_dndlog '\t' fmt_dndlog '\n'],[mat_data(1,1:7) out_dn_dlogDp out_dn_dlogDp2 out_dn_dlogDp3]);
#   fprintf(id_ou2,[fmt_out fmt_dndlog],[mat_data(1,1) mat_data(1,2:7) out_dn_dlogDp2]);
#   fprintf(id_ou3,[fmt_out fmt_dndlog],[mat_data(1,1) mat_data(1,2:7) out_dn_dlogDp3]);
end
fclose(id_in);
fclose(id_ou);
# fclose(id_ou2);
# fclose(id_ou3);
work_dir='/home/gustavo/data/soap/smps_data/';
transposed_file=['transposed_inv.txt'];
smps_data=dlmread([work_dir transposed_file],'\t',1,0);
work_file='smps_raw_data_fix_QA_full.txt';
bubble_file='bubble_flags.txt';
bubble_data=dlmread([work_dir bubble_file],'\t');
nBevents=length(bubble_data(:,1)/2);
Bubble_flag=ones(size(smps_data(:,1)));
for i=1:nBevents/2
  date1=datenum([bubble_data(i*2-1,1:5) 0]);
  date2=datenum([bubble_data(i*2,1:5) 0]);
  inx=find(and(smps_data(:,1)>=date1,smps_data(:,1)<=date2));
  length(inx)
  Bubble_flag(inx)=0;
end
# full_smps_data=[smps_data Bubble_flag];
# dlmwrite([work_dir 'flagged_file_inv.txt'],full_smps_data,'\t');


# smps_data2=dlmread([work_dir mean_file],'\t',1,0);
# smps_data3=dlmread([work_dir median_file],'\t',1,0);
nrecs=length(smps_data(:,1));
save('-hdf5',[work_dir 'smps_dataINV.h5'],'smps_data');
sample_indx=[floor(nrecs*0.25):floor(nrecs*0.75)];
subplot(3,1,1);
pcolor(smps_data(sample_indx,1),[sizes;NaN]',log10(smps_data(sample_indx,8:8+nbins).*Bubble_flag(sample_indx))');
shading('interp');
set(gca,'yscale','log');
datetick('x','keeplimits');
colorbar;
subplot(3,1,2);
pcolor(smps_data(sample_indx,1),[sizes;NaN]',log10(smps_data(sample_indx,8+nbins:8+nbins+nbins).*Bubble_flag(sample_indx))');
shading('interp');
set(gca,'yscale','log');
datetick('x','keeplimits');
colorbar;
subplot(3,1,3);
pcolor(smps_data(sample_indx,1),[sizes;NaN]',log10(smps_data(sample_indx,8+nbins+nbins:end).*Bubble_flag(sample_indx))');
shading('interp');
set(gca,'yscale','log');
datetick('x','keeplimits');
colorbar;


