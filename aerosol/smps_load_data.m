work_dir='/home/gustavo/data/soap/smps_data/';
transposed_file=['transposed_inv.txt'];
smps_data=dlmread([work_dir transposed_file],'\t',1,0);
work_file='smps_raw_data_fix_QA_full.txt';
bubble_file='bubble_flags.txt';
bubble_data=dlmread([work_dir bubble_file],'\t');
nBevents=length(bubble_data(:,1)/2);
Bubble_flag=ones(size(smps_data(:,1)));
for i=1:nBevents/2
  date1=datenum([bubble_data(i*2-1,1:5) 0]);
  date2=datenum([bubble_data(i*2,1:5) 0]);
  inx=find(and(smps_data(:,1)>=date1,smps_data(:,1)<=date2));
  length(inx)
  Bubble_flag(inx)=0;
end

full_smps_data=[smps_data Bubble_flag];
dlmwrite([work_dir 'flagged_file_inv.txt'],full_smps_data,'\t');
sample_indx=[floor(nrecs*0.1):floor(nrecs*0.9)];
figure(1);
subplot(2,1,1);
pcolor(smps_data(sample_indx,1),[sizes;NaN]',log10(smps_data(sample_indx,8:8+nbins))');
shading('interp');
set(gca,'yscale','log');
datetick('x','keeplimits');
colorbar;
subplot(2,1,2);
pcolor(smps_data(sample_indx,1),[sizes;NaN]',log10(smps_data(sample_indx,8:8+nbins).*Bubble_flag(sample_indx))');
shading('interp');
set(gca,'yscale','log');
datetick('x','keeplimits');
colorbar;
saveas(1,'/home/gustavo/Documents/Presentations/SOAP_workshop2013/smps_with_bubbles.png');