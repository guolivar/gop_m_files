function binned=group_by_size(sizes_in, sizes_low, sizes_high, left)
#  group_by_size Relates the input vectos with the bounding vectors.
#     sizes_low:  Input of "low" values of the bins (1D array length N)
#     sizes_high: Input of "high" values of the bins (1D array length N)
#     sizes_in:   Sizes that need to be "grouped" in the bins (1D array length M)
#     left:       Boolean to control the comparison
#                   TRUE      low<=size<high
#                   FALSE     low<size<=high
#     binned:     Indices of the bins that correspond to the sizes_in.
#                 binned(i)=j if sizes_in(i) is within the bin defined by
#                 sizes_low(j) and sizes_high(j) according to the left rule.
binned=zeros(size(sizes_in));
n_bins=length(sizes_low);
for i=1:length(binned)
  if left
    for j=1:n_bins
      if and(sizes_low(j)<=sizes_in(i),sizes_high(j)>sizes_in(i))
        binned(i)=j;
        break;
      end
    end
  else
    for j=1:n_bins
      if and(sizes_low(j)<sizes_in(i),sizes_high(j)>=sizes_in(i))
        binned(i)=j;
        break;
      end
    end
  end
  if sizes_in(i)<sizes_low(1)
    binned(i)=1;
  end
  if sizes_in(i)>sizes_high(end)
    binned(i)=n_bins;
  end
end