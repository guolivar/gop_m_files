function Dp=Zp2Dp(Zp,Charge,T,P)
#Calculate the equivalent electrical mobility for a given DMA voltage
# Input
#	Zp			Electrical mobility		m^2/(V*S)
#	Charge		Number of charges		--
#	T			Temperature				C
#	P			Pressure				hPa
# Output
#   Dp        Particle size				nm

#Constants
lambda0=66.40;
T0=293.15;
P0=1013;
S=110.4;
ec=1.602e-19;
my0=1.819e-5;
#Inputs
Cc=3;
T=T+273.15;
while true
	lambda=lambda0*(T/T0)*(P0/P)*((1+(S/T0))/(1+S/T));
	my=my0*((T/T0)^(1.5))*((T0+S)/(T+S));
	Dp=(Charge*ec*Cc*1e9)/(3*pi*my*Zp);
	C=1+2.492*(lambda/Dp)+0.84*(lambda/Dp)*exp(-0.43*(Dp/lambda));
	Cc=(Cc+C)/2;
	convergence=abs((C/Cc)-1);
	if convergence<0.01
		break;
	end
end
