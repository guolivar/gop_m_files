clear
fclose all;
#Invert line by line based on EMMA inverting routine
page_screen_output(0);
page_output_immediately(1);
# Set the scene
work_dir='/home/olivaresga/DATA/SOAP/smps_data/';
work_file='smps_raw_data_fix_QA_full.txt';
fixed_file=['inv_smps.txt'];
id_in=fopen([work_dir work_file],'rt');
id_out=fopen([work_dir fixed_file],'w');
# Set constants
L=44.369;
Rin=0.937;
Rout=1.961;
# Skip headers
headers=[fgetl(id_in) 9 'Dp' 9 'dnDlogDp' 9 'dN' 9 'dlogDp']
fprintf(id_out,'%s\n',headers);
# Go through the file
report=0;
indx=report;
while ~feof(id_in)
  # read data line
  lin=fgetl(id_in);
  # parse the line
  data_vec=sscanf(lin,'%f');
  indx=indx+1;
  if indx>report
    datestr(data_vec(1))
    report=report+3600*12;
  end
  # assign the parameters for the inversion
  dmaV=data_vec(12);
  dmaN=data_vec(8);
  #flowin=data_vec(18);
  flowin=1; # HARDWIRED 1lpm flow for 3010
  flowsh=10; #HARDWIRED 10lpm for tests ... otherwise use
  # 	flowsh=data_vec(13);
  P=data_vec(15);
  T=data_vec(17);
  # Test for valid data input
  if ~isnan(sum([dmaV,dmaN,L,Rin,Rout,flowin,flowsh,P,T]))
    #invert this datapoint
    [Dp dnDlogDp dN dlogDp]=invert_sample(dmaV,dmaN,L,Rin,Rout,flowin,flowsh,P,T);
  else
    Dp=NaN;
    dnDlogDp=NaN;
    dN=NaN;
    dlogDp=NaN;
  end
  #print to output file
  fprintf(id_out,'%s',lin);
  fprintf(id_out,'\t%f\t%f\t%f\t%f\n',[Dp dnDlogDp dN dlogDp]);
end
fclose(id_in);
fclose(id_out);
