#Set the scene
work_dir='/media/Win7Portable/data/soap_data/ae21_data/';
work_file='aethalometer_data.h5';
ae21=load([work_dir work_file]);
#moving average window ... in samples i.e. 5min!!!
wndw=2;
ae21.smooth_data=ae21.data(:,7:8);
for i=wndw+1:length(ae21.data(:,1))
	ae21.smooth_data(i,:)=nanmean(ae21.data(i-wndw:i,7:8));
end
# Criterion for ship contamination
limit=2500000;
clean_bc=find(ae21.smooth_data(:,1)<limit);
ae21.polluted=ae21.smooth_data(:,1)>=limit;
%~ #Set the scene
%~ work_dir='/media/Win7Portable/data/soap_data/cpc_data/';
%~ work_file='cpc_data.h5';
%~ cpc=load([work_dir work_file]);
%~ #moving average window ... in samples i.e. 1s!!!
%~ wndw=600;
%~ cpc.smooth_data=cpc.cpc_data(:,9);
%~ for i=wndw+1:length(cpc.cpc_data(:,1))
	%~ cpc.smooth_data(i,:)=nanmean(cpc.cpc_data(i-wndw:i,9));
%~ end

date1=datenum([2012 2 12 0 0 0]);
date2=datenum([2012 2 13 23 0 0]);
period=datestr((date1+date2)/2,'yyyy-mm-dd-HH');
site=['BC_SOAP' period];
#Indices to plot
plot_vec=find(and(ae21.data(:,1)>=date1,ae21.data(:,1)<=date2,(ae21.polluted==0)));
plot(ae21.data(plot_vec,1),ae21.smooth_data(plot_vec,1),'.k');
#,ae21.data(clean_bc,1),ae21.smooth_data(clean_bc,2),'.r');
legend('BC','uvBC');
datetick('x','mm/dd HH:MM');
ylabel('[ng/m^3]');
xlabel('Local Time');
saveas(gcf(),[site '.png']);
%~ close(gcf());
%~ 
%~ site=['N_SOAP' period];
%~ #Indices to plot
%~ plot_vec=find(and(cpc.cpc_data(:,1)>=date1,cpc.cpc_data(:,1)<=date2));
%~ plot(cpc.cpc_data(plot_vec,1),cpc.smooth_data(plot_vec,1),'-k');
%~ legend('N_{10}');
%~ datetick('x','mm/dd HH:MM');
%~ ylabel('[#/cm^3]');
%~ xlabel('Local Time');
%~ saveas(gcf(),[site '_LUKE_DATA.png']);
