function [Dp dnDlogDp dN dlogDp]=invert_sample(dmaV,dmaN,L,Rin,Rout,flowin,flowsh,P,T)
#Invert line by line based on EMMA inverting routine
# Input
#   dmaV		Voltage in DMA            VOLTS
#   dmaN		Number concentration RAW  	pt/cc
#   L			DMA length                	cm
#   Rin			DMA internal radius       	cm
#   Rout		DMA external radius       	cm
#   flowin		Sample flow               	l/min
#   flowsh		Sheath Flow               	l/min
#	P			Ambient pressure			mbar
# Output
#   Dp        	Particle size for voltage 	nm
#   dnDlogDp  	Inverted concentration    	pt/cc
#   dN        	Number density for size   	pt/cc
#   dlogDp    	diferential of size       	--
charges=1;
Zp=volt2zp(dmaV,L,Rin,Rout,flowsh);
Dp=Zp2Dp(Zp,charges,T,P);
Fn=bipolar(Dp,charges);
dN=dmaN*2/Fn;
flowratio=flowin/flowsh;
if isnan(flowratio)
	flowratio=0.1;
end
Zp_low=Zp*(1-flowratio);
Zp_high=Zp*(1+flowratio);
Dp_low=Zp2Dp(Zp_low,charges,T,P);
Dp_high=Zp2Dp(Zp_high,charges,T,P);
dlogDp=log10(Dp_low/Dp_high);
dnDlogDp=dN/dlogDp;
