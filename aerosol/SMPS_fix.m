fclose all
%close all
# Read the pasted SMPS file
page_screen_output(0);
page_output_immediately(1);
# Set the scene
work_dir='/home/olivaresga/DATA/SOAP/smps_data/';
work_file='smps_raw_data.txt';
fixed_file=[work_file(1:end-4) '_fix_QA.txt'];
id_in=fopen([work_dir work_file],'rt');
id_ou=fopen([work_dir fixed_file],'w');
# Write headers
first_line={'OctaveDate', ...
'Year', ...
'Month', ...
'Day', ...
'Hour', ...
'Minute', ...
'Second', ...
'3010', ...
'3010cnt1s', ...
'3010DeltaT', ...
'SetDp', ...
'DMAVolt', ...
'ShFlow', ...
'ByFlow', ...
'AbsP', ...
'ShT', ...
'CabSMPST', ...
'ImpFlow', ...
'Mobility', ...
'ShFlowSt', ...
'ByFlowSt', ...
'HVSt', ...
'ImpPresDrop', ...
'DMAmodel', ...
'GasType', ...
'Impactor', ...
'ActualDp', ...
'MinAdjDp', ...
'MaxAdjDp'};
fmt_hdr='%s';
for i=1:28
  fmt_hdr=[fmt_hdr '\t%s'];
end
fmt_hdr=[fmt_hdr '\n'];
fprintf(id_ou,fmt_hdr,first_line{:});
#format string
fmt_rd='%f';
fmt_wr='%f\t%d\t%d\t%d\t%d\t%d\t%f\t';
for i=1:21
	fmt_wr=[fmt_wr '%f\t'];
end
fmt_wr=[fmt_wr '%f\n'];
#DECIDE WHAT DATA TO GET OUT OF THE FIXING SCRIPT
#Replicate the data but eliminate the last text columns ...
#this is only to discard datapoints based on 
#SMPS and CPC diagnostic info independently
first=true;
report=0;
indx=report;
while ~feof(id_in)
  indx=indx+1;
  t_line=fgetl(id_in);
  if indx>report
    t_line(1:20)
    report=report+3600
  end
  #Is it a headers line?
  isnotheader=t_line(1)~='Y';
  islongenough=length(t_line)>150;
  if and(isnotheader,islongenough)
    data_vec=sscanf(t_line,fmt_rd);
    #Data QA
    #3010
    #deltaTemp in condenser between 16 and 18 and flow more than 0.3 lpm
    if and(data_vec(9)>=16,data_vec(9)<=18,data_vec(17)>0.3)
      #Calculate the concentration in cnts/cc
      #based on counts last second and measured flow
      data_vec(7)=data_vec(8)/(data_vec(17)*16.66667);
    else
      data_vec(7)=NaN;
    end
    #SMPS
    #critical errors are: Sheath Flow Rate (18) and HV supply (20)
    #If error, then NAN the Volt and ShFlow fields
    if or(data_vec(19)==0,data_vec(21)==0)
      data_vec(11)=NaN;
      data_vec(12)=NaN;
    end
    curr_date=datenum(data_vec(1:6)');
    data_out=[curr_date data_vec(1:28)'];
    fprintf(id_ou,fmt_wr,data_out)
    
  end
end
fclose(id_in);
fclose(id_ou);
