function Zp=Dp2Zp(Dp,Charge,T,P)
#Calculate the equivalent electrical mobility for a given DMA voltage
# Input
#   Dp        Particle size				nm
#	Charge		Number of charges		--
#	T			Temperature				C
#	P			Pressure				hPa
# Output
#	Zp			Electrical mobility		m^2/(V*S)

#Constants
lambda0=66.40;
T0=293.15;
P0=1013;
S=110.4;
ec=1.602e-19;
my0=1.819e-5;
#Inputs
T=T+273.15;

lambda=lambda0*(T/T0)*(P0/P)*((1+(S/T0))/(1+S/T));
my=my0*(((T/T0)^1.5))*((T0+S)/(T+S));
C=1+2.492*(lambda/Dp)+0.84*(lambda/Dp)*exp(-0.43*(Dp/lambda));
Zp=(Charge*ec*C*1e9)/(3*pi*my*Dp);
