fclose all
#close all
clear
#Read an AIM generated SMPS inverted file and turns it to something useful
#It plots the end result as a shaded timeseries
#Info about the file:
#When the date is exploded the number of relevant fields per line is
# 7 + NSIZES (94 sizes for MtEden and Newmarket)
#The first line contains the headers 7 for the date and Rnumber and then the centre sizes

#Set the scene
work_dir='/run/media/gustavo/Win7Portable/data/smps/Takapuna/westlake/';
work_file='westlake_aim.txt';
end_file=['smooth_' work_file];
#moving average window ... in SCANS!!!
wndw=5;
id_in=fopen([work_dir work_file],'r');
header=fgetl(id_in);
fclose(id_in);
data_in=dlmread([work_dir work_file],'\t',1,0);
#parse the bin central sizes
bin_sizes=sscanf(header(44:end),'%f');
nsizes=length(bin_sizes);
#parse the data file
data_in=dlmread([work_dir work_file],'\t',1,0);
#only deal with the size distribution part of the data
dndlogdp=data_in(:,8:8+nsizes-1);
smooth_data=zeros(size(dndlogdp));
year=2000+data_in(:,4);
nrecords=length(data_in);
date_vector=datenum([year data_in(:,[2 3 5 6 7])]);
clear data_in;
for i=1:nrecords
  if and(i<=wndw,i>2)
    smooth_data(i,:)=median(dndlogdp(1:i,:));
  elseif i<=2
    smooth_data(i,:)=mean(dndlogdp(1:i,:));
  else
    smooth_data(i,:)=median(dndlogdp(i-wndw:i,:));
  end
end
#Save hdf5 file
save('-hdf5',[work_dir end_file '.h5'],'header','dndlogdp','bin_sizes','date_vector','smooth_data');
