#fclose all;
#close all
#clear
#It plots the end result as a shaded timeseries
#The first line contains the headers 7 for the date and date_vector and then the centre sizes

#Set the scene
work_dir='/run/media/gustavo/Win7Portable/data/smps/otahuhu/SMPS_000/';
work_file='transposed_inv.txt.h5';
#load data
data=load([work_dir work_file]);
data.date_expanded=datevec(data.date_vector);
#Select the right time to summarize
#Hours
%~ hmin=7;
%~ hmax=19;
%~ leg1='07:00 - 19:00';
%~ leg2='19:00 - 07:00';
%~ h1=17;
%~ h2=19;
site='Otahuhu';
%~ period='July 2012';
%~ diurnal_summary=nanmedian(data.dndlogdp(and(data.date_expanded(:,4)>=hmin,data.date_expanded(:,4)<=hmax),:));
%~ nocturnal_summary=nanmedian(data.dndlogdp(or(data.date_expanded(:,4)<=hmin,data.date_expanded(:,4)>=hmax),:));
%~ #nocturnal_summary=nanmedian(data.dndlogdp(and(data.date_expanded(:,4)<=h2,data.date_expanded(:,4)>=h1),:));
%~ 
%~ loglog(data.bin_sizes,diurnal_summary,'-r',data.bin_sizes,nocturnal_summary,'-k');
%~ title([site ' ' period]);
%~ xlabel('D_P [nm]');
%~ ylabel('dN/dLogD_P [#/cc]');
%~ legend(leg1,leg2);
%~ saveas(gcf(),[site '_summary.png']);
%~ close(gcf());
%~ semilogx(data.bin_sizes,diurnal_summary,'-r',data.bin_sizes,nocturnal_summary,'-k');
%~ title([site ' ' period]);
%~ xlabel('D_P [nm]');
%~ ylabel('dN/dLogD_P [#/cc]');
%~ legend(leg1,leg2);
%~ saveas(gcf(),[site '_summary_LIN.png']);
%~ 
%~ diurnal_summary=diurnal_summary/sum(diurnal_summary*log10(data.bin_sizes(2)/data.bin_sizes(1)));
%~ nocturnal_summary=nocturnal_summary/sum(nocturnal_summary*log10(data.bin_sizes(2)/data.bin_sizes(1)));
%~ loglog(data.bin_sizes,diurnal_summary,'-r',data.bin_sizes,nocturnal_summary,'-k');
%~ title([site ' ' period]);
%~ xlabel('D_P [nm]');
%~ ylabel('dN/dLogD_P [#/cc]');
%~ legend(leg1,leg2);
%~ saveas(gcf(),[site '_summaryNORM.png']);
%~ close(gcf());
%~ semilogx(data.bin_sizes,diurnal_summary,'-r',data.bin_sizes,nocturnal_summary,'-k');
%~ title([site ' ' period]);
%~ xlabel('D_P [nm]');
%~ ylabel('dN/dLogD_P [#/cc]');
%~ legend(leg1,leg2);
%~ saveas(gcf(),[site '_summary_LINNORM.png']);
%~ close(gcf());





#data structure
#data.bin_sizes
#data.date_vector
#data.dndlogdp
#data.smooth_data
#moving average window ... in SCANS!!!
wndw=10;

#Plot by and hour
diurnal_summary=nanmedian(data.dndlogdp);
cycle_summary=zeros(24,length(diurnal_summary));
cycle_summary_norm=cycle_summary;
for i=1:23
	cycle_summary(i,:)=nanmedian(data.dndlogdp(data.date_expanded(:,4)==i,:));
	cycle_summary_norm(i,:)=cycle_summary(i,:)/(sum(cycle_summary(i,:))/log10(data.bin_sizes(2)/data.bin_sizes(1)));
end
cycle_summary(24,:)=nanmedian(data.dndlogdp(data.date_expanded(:,4)==0,:));
cycle_summary_norm(24,:)=cycle_summary(24,:)/(sum(cycle_summary(24,:))/log10(data.bin_sizes(2)/data.bin_sizes(1)));

figure(1);
pcolor([1:24]/24,data.bin_sizes,cycle_summary');
shading('interp');
%caxis([0 50000]);
set(gca,'yscale','log');
axis tight;
datetick('x',15,'keeplimits');
colorbar;
saveas(gcf(),[site '_summary_LIN_CYCLE.png']);
close(gcf());
figure(1);
pcolor([1:24]/24,data.bin_sizes,cycle_summary_norm');
shading('interp');
%caxis([0 50000]);
set(gca,'yscale','log');
axis tight;
datetick('x',15,'keeplimits');
colorbar;
saveas(gcf(),[site '_summary_LINNORM_CYCLE.png']);
close(gcf());

figure(1);
pcolor([1:24]/24,data.bin_sizes,log10(cycle_summary'));
shading('interp');
%caxis([0 50000]);
set(gca,'yscale','log');
axis tight;
datetick('x',15,'keeplimits');
colorbar;
saveas(gcf(),[site '_summary_LOG_CYCLE.png']);
close(gcf());
figure(1);
pcolor([1:24]/24,data.bin_sizes,log10(cycle_summary_norm'));
shading('interp');
%caxis([0 50000]);
set(gca,'yscale','log');
axis tight;
datetick('x',15,'keeplimits');
colorbar;
saveas(gcf(),[site '_summary_LOGNORM_CYCLE.png']);
close(gcf());
