function Fn=bipolar(Dp,n)
#Calculate negative charge distribution
# Input
#	Dp			Particle size		nm
#	n			Number of charges	--
# Output
#   Fn        	Charge distribution	--

if n>2
	#Multiple charge with Gunn's formula
	#Constants
	eps0=8.854E-12;
	k=1.3806E-23;
	T=293.15;
	e=1.60E-19;
	Z1=1.4;
	Z2=1.6;
	M=4*pi**2*eps0*Dp*1e-9*k*T/e**2;
	Fn=exp(-1*((-1*n-M/2/pi*log(Z1/Z2))^2)/M*pi)/sqrt(M);
else
	#Fuchs' formula
	coefficients=[-0.000300 -2.319700 -26.332800
		-0.101400 0.617500 35.904400
		0.307300 0.620100 -21.460800
		-0.337200 -0.110500 7.086700
		0.102300 -0.126000 -1.308800
		-0.010500 0.029700 0.105100];
	LDP=log10(Dp);
	Fn=0;
	for i=1:6
		Fn=Fn+coefficients(i,n+1)*LDP**i;
	end
	Fn=10**Fn;
end
