page_screen_output(0);
page_output_immediately(1);
work_dir='/home/olivaresga/data/DIDACTIC/Queen_Street/data_3775/20131130_missing/';
data_files=dir([work_dir '*.txt']);
nfiles=length(data_files);
for indx=1:nfiles
  id_in=fopen([work_dir data_files(indx).name],'rt');
  id_out=fopen([work_dir data_files(indx).name '.fix'],'wt');
  while ~feof(id_in)
    cline=fgetl(id_in);
    if (length(cline)>70)
      fprintf(id_out,'%s\n',cline);
    end
  end
  fclose(id_in);
  fclose(id_out);
end
'Done'
