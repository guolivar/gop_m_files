page_screen_output(0);
page_output_immediately(1);
%% Which file to work with
work_dir='/home/olivaresga/data/DIDACTIC/Queen_Street/tests/data_3022/';
in_file='long_test.txt';
out_file=[in_file(1:end-4) '_10min.txt'];
data_in=dlmread([work_dir in_file],'\t',1,0);
date_vec=datenum(data_in(:,1:6));
nrecs=length(date_vec);
j=1;
interval=600; #In seconds for the average
for i=601:interval:nrecs
	med_out(j)=nanmedian(data_in(i-interval:i,7));
	mean_out(j)=nanmean(data_in(i-interval:i,7));
	date_out(j)=nanmedian(date_vec(i-interval:i)');
	min_flow(j)=nanmin(data_in(i-interval:i,11));
	max_flow(j)=nanmax(data_in(i-interval:i,11));
	med_flow(j)=nanmedian(data_in(i-interval:i,11));
	j=j+1;
end
dlmwrite([work_dir out_file],[date_out med_out mean_out min_flow max_flow med_flow],'\t');
# Plot concentration
figure(1);
plot(date_out,med_out,'-k',date_out,mean_out,'-r');
title({'CPC 3022A PNC', '10 minute average'});
legend('Median','Mean');
datetick;
saveas(1,[work_dir 'N4.png']);

# Plot flow
figure(2);
plot(date_out,min_flow,'-g',date_out,med_flow,'-k',date_out,max_flow,'-r');
title('CPC 3022A flow status');
legend('Min','Median','Max');
datetick;
saveas(1,[work_dir 'Flow.png']);