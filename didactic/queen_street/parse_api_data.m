clear all
# fclose all
# close all
# Read the "semi-raw" AC gas data
page_screen_output(0);
page_output_immediately(1);
# Set the scene
work_dir='/home/gustavo/GDrive/Work/PENAP/Data Analysis/';
work_file='gases_qs_May2014.txt';
co_load_file=['co_load.txt'];
no_load_file=['no_load.txt'];
no2_load_file=['no2_load.txt'];
nox_load_file=['nox_load.txt'];
id_in=fopen([work_dir work_file],'rt');
id_co=fopen([work_dir co_load_file],'w');
id_no=fopen([work_dir no_load_file],'w');
id_no2=fopen([work_dir no2_load_file],'w');
id_nox=fopen([work_dir nox_load_file],'w');
# Info for the input queries
# siteid = 1
# parameterid = [465 = CO | 466 = NO | 467 = NO2 | 468 = NOx]
# Insert template
# INSERT INTO data.fixedmeasurements(
#    year, month, day, hour, minute, second, parameterid, siteid, 
#    recordtime, value)
# VALUES (?, ?, ?, ?, ?, ?, ?, 1, 
#    ?, ?);
prefix="INSERT INTO data.fixedmeasurements(year, month, day,hour,minute, second, parameterid, siteid,recordtime, value) VALUES";
cline=fgetl(id_in); #Drop the header line in the input file
fprintf(id_co,'%s\n',prefix);
fprintf(id_no,'%s\n',prefix);
fprintf(id_nox,'%s\n',prefix);
fprintf(id_no2,'%s\n',prefix);
while ~feof(id_in)
  # Read current line
  cline=fgetl(id_in);
  # Parse current line
  c_vec=sscanf(cline,'%d-%d-%d %d:%d:%d\t%f\t%f\t%f\t%f')';
  yr=c_vec(1);
  mo=c_vec(2);
  dy=c_vec(3);
  hr=c_vec(4);
  mi=c_vec(5);
  se=c_vec(6);
  # Generate timestamp
  timestamp=datestr(datenum([yr mo dy hr mi se]),",'YYYY-mm-DD HH:MM:SS NZST',");
  # Extract gas concentrations
  co=c_vec(7);
  no=c_vec(8);
  nox=c_vec(9);
  no2=c_vec(10);
  # Output statements to each file
  #CO
  if ~isnan(co)
	  line_out=[sprintf('(%d,%d,%d,%d,%d,%d,465,1',yr,mo,dy,hr,mi,se) ...
		timestamp sprintf('%f),',co)];
	  fprintf(id_co,'%s\n',line_out);
  end
  #NO
  if ~isnan(no)
	line_out=[sprintf('(%d,%d,%d,%d,%d,%d,466,1',yr,mo,dy,hr,mi,se) ...
		timestamp sprintf('%f),',no)];
	fprintf(id_no,'%s\n',line_out);
  end
  #NOx
  if ~isnan(nox)
	line_out=[sprintf('(%d,%d,%d,%d,%d,%d,467,1',yr,mo,dy,hr,mi,se) ...
		timestamp sprintf('%f),',nox)];
	fprintf(id_nox,'%s\n',line_out);
  end
  #NO2
  if ~isnan(no2)
	line_out=[sprintf('(%d,%d,%d,%d,%d,%d,468,1',yr,mo,dy,hr,mi,se) ...
		timestamp sprintf('%f),',no2)];
	fprintf(id_no2,'%s\n',line_out);
  end
end
fclose(id_in)
fclose(id_co)
fclose(id_no)
fclose(id_no2)
fclose(id_nox)


