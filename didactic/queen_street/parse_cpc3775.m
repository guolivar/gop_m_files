page_screen_output(0);
page_output_immediately(1);
%% Which file to work with
work_dir='/home/olivaresga/data/DIDACTIC/Queen_Street/data_3775/logged_data_May2014/';
id_out=fopen([work_dir 'to_database.sql'],'wt');
filein='data_hole.txt';
id_n4=408;
id_error=409;
id_site=2;
fprintf(id_out,'%s\n','INSERT INTO data.fixeddata (year,month,day,hour,minute,second,parameterid,value,siteid,recordtime) VALUES');
id_in=fopen([work_dir filein],'rt');
ii=0;
while ~feof(id_in)
  r_conc=zeros(30,1);
  for minute=1:30
    cline=fgetl(id_in);
    data_vec=sscanf(cline,'%d,%d,%d,%d,%d,%d,%e');
    r_conc(minute)=data_vec(7);
  end
  conc=mean(r_conc);
  errors=cline(30:end);
  c_date=data_vec(1:6)';
  ii=ii+1;
  # output lines to SQL script
  # Check proper timezone
  fprintf(id_out,'%s','(');
  fprintf(id_out,"%d,%d,%d,%d,%d,%d,%d,'%d',%d,",[c_date id_n4 conc id_site]);
  fprintf(id_out,'%s),\n',['timestamptz ' datestr(datenum(c_date),"'%Y-%m-%d %H:%M:%S NZST'")]);
  
  fprintf(id_out,'%s','(');
  fprintf(id_out,"%d,%d,%d,%d,%d,%d,%d,'",[c_date id_error]);
  fprintf(id_out,"%s',",errors);
  fprintf(id_out,'%d,',id_site);
  fprintf(id_out,'%s),\n',['timestamptz ' datestr(datenum(c_date),"'%Y-%m-%d %H:%M:%S NZST'")]);

end
fclose(id_in);
fclose(id_out);
