page_screen_output(0);
page_output_immediately(1);
# Set the scene
work_dir='/home/olivaresga/data/DIDACTIC/Queen_Street/data_3775/from_logging_PC/';
data_files=dir([work_dir '*.fix']);
nfiles=length(data_files);
id_out=fopen([work_dir 'data_full.out'],'wt');
for indx=1:nfiles
  data_files(indx).name
  id_in=fopen([work_dir data_files(indx).name],'rt');
  startdate=datenum(data_files(indx).name(1:10),'YYYYmmDDHH');
  t_indx=0;
  while ~feof(id_in)
    cline=fgetl(id_in);
    current_date=startdate+t_indx/(24*3600);
    data_line=[datestr(current_date,"YYYY,mm,DD,HH,MM,SS,'YYYY-mm-DD HH:MM:SS NZST',") cline];
    if (length(cline)>10)
      fprintf(id_out,'%s\n',data_line);
    end
    t_indx=t_indx+1;
  end
  fclose(id_in);
end
fclose(id_out);
