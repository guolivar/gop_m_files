page_screen_output(0);
page_output_immediately(1);
# Parse raw PTrak data (csv) into DIDACTIC friendly format
# Including the "Octave date"
# Set the scene
work_dir='/home/olivaresga/data/DIDACTIC/CAMPAIGN/RAW/20130919-1/';
id_files=fopen([work_dir 'langan_files.lst'],'rt');
while ~feof(id_files)
	curr_file=fgetl(id_files);
  sep=strfind(curr_file(1:end-4),'_');
  inst_id=curr_file(sep(end)+1:end-4);
  id_in=fopen([work_dir curr_file],'rt');
  output_file=['Langan_' inst_id '.txt']
  id_out=fopen([work_dir output_file],'at');
  fmt_data_in='%02d-%02d-%02d,%02d:%02d:%02d,%f,%f';
  fmt_data_out='%.7f\t%.7f\t%.7f\t%s\n';
  # Discard first line
  c_line=fgetl(id_in);
  # Process the rest of the file
  while ~feof(id_in);
    c_line=fgetl(id_in);
    if length(strfind(c_line,'Logged'))==0
      data_vec=sscanf(c_line,fmt_data_in);
      date_out=datenum([2000 0 0 0 0 0]+data_vec([3 2 1 4 5 6])');
      timestamp=[datestr(date_out,31) ' NZST'];
      conc_out=data_vec(7);
      temp_out=data_vec(8);
      fprintf(id_out,fmt_data_out,date_out, conc_out, temp_out,timestamp);
    end
  end
  fclose(id_in);
  fclose(id_out);
end
fclose(id_files);
