page_screen_output(0);
page_output_immediately(1);
# Parse raw ALL data (csv) into DIDACTIC friendly format
# Including the "Octave date" fields and corrections for N/S and E/W
# Set the scene
work_dir='/home/olivaresga/data/DIDACTIC/CAMPAIGN/RAW/20130919-1/';
process_file='gps_files.lst';
id_list=fopen([work_dir process_file],'rt');
while ~feof(id_list)
  file_in=fgetl(id_list)
  sep=strfind(file_in,'_');
  input_GPS=['GPS-G' num2str(sscanf(file_in(sep(end)+1:end),'%d'))];
  output_file=[input_GPS '.txt']
  id_in=fopen([work_dir file_in],'rt');
  id_ou=fopen([work_dir output_file],'at');
  # Data output fields
  header={'OctaveTime',
    'speed',
    'heading'};
  # fprintf(id_ou,'%s\t%s\t%s\t%s\t%s\t%s\t%s\n',header{:});
  data_out=cell(1,3);
  data_in=cell(1,18);
  # Format for data out
  fmt_out='%.7f\t%s\t%s\t%s\n';
  # Read first GPS line and ignore it
  c_lin=fgetl(id_in);
  while ~feof(id_in)
    # Read current line and parse
    c_lin=fgetl(id_in);
    sep=strfind(c_lin,',');
    jj=1;
    for ii=1:length(sep)
      data_in{ii}=c_lin(jj:sep(ii)-1);
      jj=sep(ii)+1;
    end
    data_in{ii+1}=c_lin(sep(ii)+1:end);
    # Get time in Octave DATENUM format
    timestamp=[data_in{6} ' ' data_in{7} ' NZST'];
    octavetime=datenum([data_in{6} ' ' data_in{7}],'yyyy/mm/dd HH:MM:SS');
    # Speed
    speed=data_in{14};
    # Heading
    heading=data_in{15};
    fprintf(id_ou,fmt_out,octavetime, speed, heading,timestamp);
  end
  fclose(id_ou);
  fclose(id_in);
end
fclose(id_list);
