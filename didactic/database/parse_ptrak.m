# Parse raw PTrak data (csv) into DIDACTIC friendly format
# Including the "Octave date"
# Set the scene
work_dir='/home/olivaresga/data/DIDACTIC/CAMPAIGN/RAW/20130919-1/';
id_files=fopen([work_dir 'ptrak_files.lst'],'rt');
while ~feof(id_files)
	input_file=fgetl(id_files)
	id_in=fopen([work_dir input_file],'rt');
	fmt_data_in='%02d/%02d/%04d\t%02d:%02d:%02d\t%d';
	fmt_data_out='%.7f\t%.7f\t%s\n';
	c_line=fgetl(id_in);
	while length(strfind(c_line,'Serial Number'))==0
	  c_line=fgetl(id_in);
	end
	# Parse serial number line
	id1=strfind(c_line,'-');
	snumber=c_line(id1+1:end);
	output_file=['PTrak_' snumber '.txt'];
	id_out=fopen([work_dir output_file],'at');
	# Look for the "Date" line
	c_line=fgetl(id_in);
	while length(strfind(c_line,'dd/MM/yyyy'))==0
	  c_line=fgetl(id_in);
	end
	# Ignore these 2 lines (headers for data)
	c_line=fgetl(id_in);
	# Process the rest of the file
	while ~feof(id_in);
	  c_line=fgetl(id_in);
	  if length(strfind(c_line,'valid'))==0
		  data_vec=sscanf(c_line,fmt_data_in);
		  date_out=datenum(data_vec([3 2 1 4 5 6])');
		  conc_out=data_vec(7);
		  timestamp=[datestr(date_out,31) ' NZST'];
		  fprintf(id_out,fmt_data_out,date_out, conc_out,timestamp);
	  end
	end
	fclose(id_in);
	fclose(id_out);
end
fclose(id_files);
