page_screen_output(0);
page_output_immediately(1);
# Add YEAR MONTH DAY HOUR MINUTE SECOND columns to the file to update.
# Original columns
header={'lat','lon','elevation','gpsdate','person','instrument','route'};
# Original format
fmt_in='%.7f\t%.7f\t%.7f\t%.7f\t%s\t%s\t%s\n';
# New format
fmt_out='%04d\t%02d\t%02d\t%02d\t%02d\t%06.4f\t%s\n';
process_file='/home/gustavo/data/didactic/CAMPAIGN/workdir/all_to_27Aug.txt';
id_in=fopen(process_file,'rt');
id_out=fopen([process_file '.new'],'w');
i=0
next=5000;
prevT=time();
while ~feof(id_in)
  clin=fgetl(id_in);
  tmp1=sscanf(clin,'%f');
  dv=datevec(tmp1(4));
  fprintf(id_out,'%04d\t%02d\t%02d\t%02d\t%02d\t%06.4f\t%s\n',dv,clin);
  if i==next
    i
    currT=time();
    900000*(currT-prevT)/5000
    next=next+5000;
    prevT=currT;
  end
  i=i+1;
end
fclose(id_in);
fclose(id_out);
