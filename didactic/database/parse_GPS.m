page_screen_output(0);
page_output_immediately(1);
# Parse raw ALL data (csv) into DIDACTIC friendly format
# Including the "Octave date" fields and corrections for N/S and E/W
# Set the scene
work_dir='/home/olivaresga/data/DIDACTIC/CAMPAIGN/RAW/20130919-1/';
process_file='process_me.txt';
# Open the list file
id_list=fopen([work_dir process_file],'rt');
while ~feof(id_list)
  runinfo=fgetl(id_list);
  sep=strfind(runinfo,char(9));
  input_GPS=runinfo(1:sep(1)-1);
  person=runinfo(sep(1)+1:sep(2)-1);
  route=runinfo(sep(2)+1:sep(3)-1);
  if length(sep)==4
#     NO PTRAK
    instruments={runinfo(sep(3)+1:sep(4)-1),runinfo(sep(4)+1:end)}
  elseif length(sep)==5
#     With PTRAK
    instruments={runinfo(sep(3)+1:sep(4)-1),runinfo(sep(4)+1:sep(5)-1),runinfo(sep(5)+1:end)}
  elseif length(sep)==6
#     With 2 PTRAKs
    instruments={runinfo(sep(3)+1:sep(4)-1),runinfo(sep(4)+1:sep(5)-1),runinfo(sep(5)+1:sep(6)-1),runinfo(sep(6)+1:end) }
  else
  
#   Something wrong ... freak out!
    break;
  end
  output_file=['gps_to_db.txt'];
  id_in=fopen([work_dir input_GPS],'rt');
  id_ou=fopen([work_dir output_file],'at');
  # Data output fields

  # 'The GPS gives me location and record for this table.
  # Each record is also attached to the Instrument IDs that were used
  # <position_info><instrumentID1>
  # <position_info><instrumentID2>
  # <position_info><instrumentID3>
  # <position_info><instrumentID4>
  #
  #   1 Year		=	Year NUMBER
  #   2 Month		=	Month NUMBER
  #   3 Day		=	Day NUMBER
  #   4 Hour		=	Hour NUMBER
  #   5 Minute		=	Minute NUMBER
  #   6 Second		=	Second NUMBER
  #   7 lat		=	Latitude, NUMBER
  #   8 lon		=	Longitude, NUMBER
  #   9 elevation	=	Terrain height, NUMBER
  #   10 gpsdate	=	Local time OCTAVE DATENUM
  #   11 person	=	Name of person sampling, STRING
  #   12 instrument	=	SerialN of the instrument used, STRING
  #   13 route	=	Route name, STRING
  #   14 TIMESTAMP	=	Timestamp from the GPSDATE field STRING

  header={'lat',
    'lon',
    'elevation',
    'gpsdate',
    'person',
    'instrument',
    'route'};
  # fprintf(id_ou,'%s\t%s\t%s\t%s\t%s\t%s\t%s\n',header{:});
  data_out=cell(1,14);
  data_in=cell(1,18);
  # Format for data out
  fmt_out='%04d\t%02d\t%02d\t%02d\t%02d\t%06.4f\t%.7f\t%.7f\t%.7f\t%.7f\t%s\t%s\t%s\t%s\n';
  # Read first GPS line and ignore it
  c_lin=fgetl(id_in);
  while ~feof(id_in)
    # Read current line and parse
    c_lin=fgetl(id_in);
    sep=strfind(c_lin,',');
    jj=1;
    for ii=1:length(sep)
      data_in{ii}=c_lin(jj:sep(ii)-1);
      jj=sep(ii)+1;
    end
    data_in{ii+1}=c_lin(sep(ii)+1:end);
    # Deal with E/W and N/S lat/lon position
    if data_in{10}=='S'
      data_out{7}=-str2num(data_in{9});
    else
      data_out{7}=str2num(data_in{9});
    end
    if data_in{12}=='W'
      data_out{8}=-str2num(data_in{11});
    else
      data_out{8}=str2num(data_in{11});
    end
    # Get elevation
    data_out{9}=str2num(data_in{13});
    # Get time in Octave DATENUM format
    data_out{10}=datenum([data_in{6} ' ' data_in{7}],'yyyy/mm/dd HH:MM:SS');
    data_out_vec=datevec(data_out{10});
    data_out{1}=data_out_vec(1);
    data_out{2}=data_out_vec(2);
    data_out{3}=data_out_vec(3);
    data_out{4}=data_out_vec(4);
    data_out{5}=data_out_vec(5);
    data_out{6}=data_out_vec(6);
    data_out{14}=[datestr(data_out{10},'yyyy/mm/dd HH:MM:SS.FFF') ' NZST'];
    # Person
    data_out{11}=person;
    # Route
    data_out{13}=route;
    # Instruments
    for i=1:length(instruments)
      # Output with instrument SNr
      data_out{12}=instruments{i};
      fprintf(id_ou,fmt_out,data_out{:});
    end
  end
  fclose(id_ou);
  fclose(id_in);
end
fclose(id_list);