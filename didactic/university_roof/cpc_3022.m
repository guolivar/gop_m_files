## Which file to work with                                                                                                                                                          
work_dir='/home/olivaresga/data/DIDACTIC/UniversityRoof/';
out_file=[work_dir 'cpc3022_1min.txt'];
id_out=fopen(out_file,'wt');
in_file=fopen([work_dir 'cpc3022.out'],'rt');
data_v=NaN*ones(600,14);
# Try with 600 samples at a time (10min approximation)
while ~feof(in_file)
  for i=1:600
    cline=fgetl(in_file);
    data_v(i,:)=sscanf(cline(28:end),'%f,')';
  end
  summary_v=nanmedian(data_v);
  fprintf(id_out,'%s%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n',cline(1:28),summary_v);
  data_v=NaN*data_v;
end
fclose(in_file);
fclose(id_out);