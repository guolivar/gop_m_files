page_screen_output(0);
page_output_immediately(1);
## Generate SQL file to load GRIMM data to DIDACTIC
parameterid=[434 435 436 437 438 439 440 441 442 443 444 445 446 447 448 449 450 451 452 453 454 455 456 457 458 459 460 461 462 463 464]; #id in admin.sensors for the GRIMM channels
fid_in=fopen('/home/gustavo/data/didactic/FixedSites/UniversityRoof/grimm_data_full.txt','rt');
fid_ou=fopen('/home/gustavo/data/didactic/FixedSites/UniversityRoof/grimm_data_DB.sql','wt');
#Skip first line in file
cline=fgetl(fid_in);
prefix="insert into data.fixedmeasurements (siteid,year,month,day,hour,minute,second,recordtime,parameterid,value) values(3,";
#At the university roof SITEID=3
ii=0;
while ~feof(fid_in)
  ii=ii+1;
  #Get current line
  cline=fgetl(fid_in);
  #parse current line
  cvec=sscanf(cline,'%f');
  cvec(2)=cvec(2)+2000;
  #construct date string
  dstring=datestr(datenum(cvec(2:7)'),"yyyy,mm,dd,HH,MM,SS,'yyyy-mm-dd HH:MM:SS NZST',");
  #skip no-data (error!=0)
  if cvec(8)==0
    #construct the insert commands
    for i=1:length(parameterid)
      sqlstr=[prefix dstring num2str(parameterid(i),'%d') ',' num2str(cvec(i+8),'%.3f') ');'];
      st=fprintf(fid_ou,'%s\n',sqlstr);
    end
    if ii>10000
      sqlstr
      ii=0;
    end
  end
end
