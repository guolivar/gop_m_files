page_screen_output(0);
page_output_immediately(1);
%% Which file to work with
work_dir='/home/gustavo/data/didactic/FixedSites/UniversityRoof/data_3022/';
Ddate=3891.4;
aa=dir([work_dir '*.dat']);
out_file=[work_dir 'all_campaign.txt'];
id_out=fopen(out_file,'wt');
nfiles=length(aa);
for i=1:nfiles
  disp(sprintf('The current file is: %s',aa(i).name));
  in_file=fopen([work_dir aa(i).name],'rt');
  while ~feof(in_file)
    cline=fgetl(in_file);
    while cline(1)=='Y'
      cline=fgetl(in_file);
    end
    date_v_o=sscanf(cline,'%f',6)';
    sep=strfind(cline,char(9));
    if date_v_o(1)<2010
      c_date=datenum(date_v_o)+Ddate;
    else
      c_date=datenum(date_v_o);
    end
    date_v_c=sprintf('%.7f\t%04d\t%02d\t%02d\t%02d\t%02d\t%05.3f\t',[c_date datevec(c_date)]);
    fprintf(id_out,'%s\n',[date_v_c cline(sep(6)+1:end)]);
  end
  fclose(in_file);
end
fclose(id_out);