page_screen_output(0);
page_output_immediately(1);
# Set the scene
work_dir='/home/gustavo/data/didactic/queenst/data_3775/';
data_files=dir([work_dir '*.fix']);
nfiles=length(data_files);
id_out=fopen([work_dir 'data_full.out'],'wt');
id_out2=fopen([work_dir 'errors_full.out'],'wt');
t_indx=0;
next_T=1000;
for indx=1:nfiles
  data_files(indx).name
  id_in=fopen([work_dir data_files(indx).name],'rt');
  while ~feof(id_in)
    current_date=0;
    current_N=0;
    for xi=1:30
      cline=fgetl(id_in);
      if feof(id_in)
	break;
      end
      cvect=sscanf(cline,'%f,');
      current_date=current_date+datenum(cvect(1:6)');
      current_N=current_N+cvect(7);
    end
    if feof(id_in)
      break;
    end
    date_string=datestr(current_date/30,"YYYY,mm,DD,HH,MM,SS,'YYYY-mm-DD HH:MM:SS NZST',");
    current_N=current_N/30;
#     (2013,11,3,22,2,30,408,16300,2,timestamptz 'Sun Nov 03 22:01:31 2013 CST' + interval '2 seconds'),
    data_line=['INSERT INTO data.fixeddata (year,month,day,hour,minute,second,recordtime,parameterid,value,siteid) VALUES (' date_string "408," num2str(cvect(7)) ",2);"];
    error_line=['INSERT INTO data.fixeddata (year,month,day,hour,minute,second,recordtime,parameterid,value,siteid) VALUES (' date_string "409,'" cline(30:end) "',2);"];
    fprintf(id_out,'%s\n',data_line);
    fprintf(id_out2,'%s\n',error_line);
    t_indx=t_indx+1;
    if t_indx>next_T
      disp(date_string)
      next_T=next_T+1000;
    end
  end
  fclose(id_in);
end
fclose(id_out);
fclose(id_out2);