%% Read AGOR file and pad the missing data
page_screen_output(0);
page_output_immediately(1);
%% Which file to work with
work_dir='/home/olivaresga/DATA/breambay/fieldtests/';
in_file='CON260213_7.CSV';
fid_in=fopen([work_dir in_file],'r');
ou_file='alltogether_20120823_full.txt';
fid_ou=fopen([work_dir ou_file],'w');
strin=fgetl(fid_in);
strou=['Date_octave,' strin];
fprintf(fid_ou,'%s\n',strou);
strin=fgetl(fid_in);
d_vec=sscanf(strin,'%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f')';
date_num=datenum(d_vec(2:7));
next_date_num=date_num+1/(24*3600);
fprintf(fid_ou,'%.8f,%s\n',date_num,strin);
curr_d_vec=d_vec(2:7)+[0 0 0 0 0 1];
curr_date_num=datenum(curr_d_vec);
pad_vec=[NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN];
endprogram=0;
err_nelements=0;
err_RTC=0;
err_same_record=0;
while ~feof(fid_in)
	strin=fgetl(fid_in);
	p_d_vec=d_vec;
	d_vec=sscanf(strin,'%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f')';
	%establish error condition
	%Number of elements in the record must be 11 
	err_nelements=length(d_vec)~=11;
	%Valid date
	if length(d_vec)>7
		% (catching error 165 from the RTC) ... date above 1999
		err_RTC=d_vec(2)<=1999;
		%repeated records
		test_date=datenum(d_vec(2:7));
		err_same_record=p_d_vec(2:7)==d_vec(2:7);
		if err_same_record
			printf('%s\n','Repeated');
		end
	end
	while or(err_nelements,err_RTC,err_same_record)
		strin=fgetl(fid_in);
		d_vec=sscanf(strin,'%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f')';
		%establish error condition
		%Number of elements in the record must be 11 
		err_nelements=length(d_vec)~=11;
		%Valid date
		if length(d_vec)>7
			% (catching error 165 from the RTC) ... date above 1999
			err_RTC=d_vec(2)<=1999;
			%repeated records
			test_date=datenum(d_vec(2:7));
			err_same_record=p_d_vec(2:7)==d_vec(2:7);
			if err_same_record
				printf('%s\n','Repeated');
			end
		end
	end
	next_date_num=datenum(d_vec(2:7));
	while next_date_num>curr_date_num
		fprintf(fid_ou,'%.8f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n',[curr_date_num pad_vec]);
		curr_d_vec=curr_d_vec+[0 0 0 0 0 1];
		curr_date_num=datenum(curr_d_vec);
	end
	fprintf(fid_ou,'%.8f,%s\n',curr_date_num,strin);
	curr_d_vec=curr_d_vec+[0 0 0 0 0 1];
	curr_date_num=datenum(curr_d_vec);
end
fclose(fid_in);
fclose(fid_ou);
data=dlmread([work_dir ou_file],',',1,0);
subplot(3,1,1);
plot(data(:,1),data(:,9));
datetick;
ylabel('Vibration 1');
subplot(3,1,2);
plot(data(:,1),data(:,10));
datetick;
ylabel('Vibration 2');
subplot(3,1,3);
plot(data(:,1),data(:,11));
datetick;
xlabel('Local Time');
ylabel('Vibration 3');
saveas(gcf,[work_dir 'summary_vibrations.png']);