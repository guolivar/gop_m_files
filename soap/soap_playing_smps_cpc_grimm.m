smps_dir='/home/olivaresga/DATA/SOAP/smps_data/';
smps_file='transposed_inv_QA_MEDIAN.txt';
cpc_dir='/home/olivaresga/DATA/SOAP/cpc_data/';
cpc_file='cpc_data.h5';
grimm_dir='/home/olivaresga/DATA/SOAP/grimm_data/';
grimm_file='coarse_spectra.h5';
aethalometer_dir='/home/olivaresga/DATA/SOAP/ae21_data/';
aethalometer_file='aethalometer_data.h5';
smps_data=dlmread([smps_dir smps_file],'\t',1,0);
cpc=load([cpc_dir cpc_file]);
grimm=load([grimm_dir grimm_file]);
ae21=load([aethalometer_dir aethalometer_file]);
# SMPS sizes
nrecs_smps=length(smps_data(:,1));
nbins=30;
dmin=8;
dmax=300;
scaling=(dmax/dmin)^(1/(nbins-1));
sizes=ones(nbins,1);
sizes(1)=dmin;
fmt_out='%f\t%f\t%f\t%f\t%f\t%f\t%f\t';
fmt_dndlog='%f';
for i=2:nbins
  sizes(i)=sizes(i-1)*scaling;
  fmt_dndlog=[fmt_dndlog '\t%f'];
end
sizes_low=sizes/sqrt(scaling);
sizes_high=sizes*sqrt(scaling);
# Integrate SMPS
Ndma=NaN*zeros(nrecs_smps,1);
for i=1:nrecs_smps
  Ndma(i)=0.054277*nansum(smps_data(i,2:2+nbins-1));
end
bubble_file='bubble_flags_ext.txt';
bubble_data=dlmread([smps_dir bubble_file],'\t');
nBevents=length(bubble_data(:,1)/2);
Bubble_flag=ones(size(smps_data(:,1)));
for i=1:nBevents/2
  date1=datenum([bubble_data(i*2-1,1:5) 0]);
  date2=datenum([bubble_data(i*2,1:5) 0]);
  inx=find(and(smps_data(:,1)>=date1,smps_data(:,1)<=date2));
  length(inx)
  Bubble_flag(inx)=NaN;
end

#moving average window ... in samples i.e. 5min!!!
wndw=6;
ae21.smooth_data=ae21.aethalometer_data(:,7:8);
for i=wndw+1:length(ae21.aethalometer_data(:,1))
	ae21.smooth_data(i,:)=nanmean(ae21.aethalometer_data(i-wndw:i,8:9));
end

figure(1);
subplot(2,1,1);
plot(smps_data(:,1),Ndma,'-g')
subplot(2,1,2);
plot(cpc.cpc_data(:,1),cpc.cpc_data(:,9),'-r');

figure(2);
plot(cpc.cpc_data(:,1),cpc.cpc_data(:,9),'-r',smps_data(:,1),Ndma,'-g',ae21.aethalometer_data(:,1),ae21.smooth_data(:,1),'-k');
ylim([0 5000]);