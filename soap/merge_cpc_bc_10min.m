#Set the scene for BC
work_dir='/home/gustavo/data/soap/';
work_file='aethalometer_data.h5';
ae21=load([work_dir work_file]);
#moving average window ... in samples i.e. 5min!!!
wndw=1 ;
ae21.smooth_data=ae21.aethalometer_data(:,7:8);
for i=wndw+1:length(ae21.aethalometer_data(:,1))
	ae21.smooth_data(i,:)=nanmean(ae21.aethalometer_data(i-wndw:i,7:8));
end
# Criterion for ship contamination
limit=2500000;
clean_bc=find(ae21.smooth_data(:,1)<limit);
ae21.polluted=ae21.smooth_data(:,1)>=limit;
#Set the scene for N10
work_dir='/home/gustavo/data/soap/';
work_file='cpc_dataV2.h5';
load([work_dir work_file]);
#moving average window ... in samples i.e. 1s!!!
wndw=900;
cpc.smooth_data=cpc.cpc_data(:,9);
for i=wndw+1:length(cpc.cpc_data(:,1))
  cpc.smooth_data(i,:)=nanmean(cpc.cpc_data(i-wndw:i,9));
end
# start with the BC data, get the date, then average the previous wndw CPC records if there are that many
n_bc=length(ae21.smooth_data);
bc_n=NaN*ones(n_bc,3);
for i=1:n_bc
  ind_cpc=find(abs(cpc.cpc_data(:,1)-ae21.aethalometer_data(i,1))<2/(24*3600));
  if ind_cpc>wndw
    bc_n(i,1)=ae21.smooth_data(i,1);
    bc_n(i,2)=ae21.smooth_data(i,2);
    bc_n(i,3)=cpc.smooth_data(ind_cpc(1),1);
  end
end
merged_data=struct('timestamp',[],'data',[]);
merged_data.timestamp=ae21.aethalometer_data(:,1);
merged_data.data=bc_n;
save('-hdf5','/home/gustavo/data/soap/bc_n.h5','merged_data');
id_out=fopen('/home/gustavo/data/soap/bc_n.txt','wt')
nfin=length(merged_data.timestamp)
for inx=1:nfin
  cdate=datestr(merged_data.timestamp(inx),"'yyyy-mm-dd HH:MM:SS'");
  st=fprintf(id_out,'%s\t%f\t%f\t%f\n',cdate,merged_data.data(inx,1),merged_data.data(inx,2),merged_data.data(inx,3));
end 
fclose(id_out);
%dlmwrite('/home/gustavo/data/soap/bc_n.txt',[ae21.aethalometer_data(:,1) bc_n],'\t');
