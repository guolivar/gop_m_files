# work_dir='/home/olivaresga/DATA/SOAP/cpc_data/';
# cpc=load([work_dir 'cpc_data.h5']);

# Assuming that the data is in:
# cpc.cpc_data as loaded by cpc=load('cpc_data.h5');
date1=datenum([2012 3 4 8 10 0]);
date2=datenum([2012 3 4 8 20 0]);
# date1=min(cpc.cpc_data(:,1));
# date2=max(cpc.cpc_data(:,1));
period=datestr((date1+date2)/2,'yyyy-mm-dd');
#Indices to plot
plot_vec=find(and(cpc.cpc_data(:,1)>=date1,cpc.cpc_data(:,1)<=date2)); #Finds the indices that fall within the dates 1 and 2
#plot
plot(cpc.cpc_data(plot_vec,1),cpc.cpc_data(plot_vec,8),'-k',cpc.cpc_data(plot_vec,1),cpc.cpc_data(plot_vec,9),'-r');
#make the plot look nice
# ylim([0 10000]);
# set(gca,'yscale','log');
legend('CPC3007','CPC3772');
datetick(); #to put the x axis in human readable format
ylabel('[#/cm^3]');
xlabel('Local Time');

