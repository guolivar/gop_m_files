# Pad a pasted file according to the timestep selected
# fclose all
# close all
# Read the pasted CPC file (daily)
page_screen_output(0);
page_output_immediately(1);
# Set the scene
work_dir='/home/olivaresga/DATA/SOAP/cpc_data/';
work_file='cpc_raw_data_fix.txt';
fixed_file=[work_file(1:end-7) '_full.txt'];
id_in=fopen([work_dir work_file],'rt');
id_ou=fopen([work_dir fixed_file],'w');
# Format for data read ... and write
fmt='%f\t%d\t%d\t%d\t%d\t%d\t%f\t%f\t%f';
# pad record ... no timestamp
pad_vec=[NaN NaN];
# Timestep expected ... in vector form [Yr Mo Dy Hr Mi Se]
timestep=[0 0 0 0 0 1];
# Write headers
t_line=fgetl(id_in);
fprintf(id_ou,'%s\n',t_line);
# Read first line and set starting date
t_line=fgetl(id_in);
data_vec=sscanf(t_line,fmt)';
curr_date_num=datenum(data_vec(2:7)+timestep);
fprintf(id_ou,'%s\n',t_line);
imworking=0;
notice=imworking+5000;
while ~feof(id_in)
  imworking=imworking+1;
  if imworking>=notice
    disp(['I am still at it, be patient ... ' num2str(notice)]);
    notice=notice+5000;
  end
  #read next data line
  t_line=fgetl(id_in);
  data_vec=sscanf(t_line,fmt)';
  #get DATE to which this record belongs
  next_date_num=datenum(data_vec(2:7));
  #Compare the record_date with the expected date. If the record date is into the future,
  #output a PAD line to the file and increase expected date value.
  while next_date_num>curr_date_num
    fprintf(id_ou,[fmt '\n'],[curr_date_num datevec(curr_date_num) pad_vec]);
    curr_date_num=curr_date_num+datenum(timestep);
    'missing data'
  end
  fprintf(id_ou,[fmt '\n'],[curr_date_num  datevec(curr_date_num) data_vec(8:9)]);
  curr_date_num=curr_date_num+datenum(timestep);
end
fclose(id_in);
fclose(id_ou);
cpc_data=dlmread([work_dir fixed_file],'\t',1,0);
cpc_headers={'OctaveDate','Year','Month','Day','Hour','Minute','Second','CPC3007','CPC3772'};
#moving average window ... in samples i.e. 1s!!!
wndw=60;
s_data=cpc_data(:,8:9);
for i=wndw+1:length(cpc_data(:,1))
	s_data(i,1)=nanmean(cpc_data(i-wndw:i,8));
	s_data(i,2)=nanmean(cpc_data(i-wndw:i,9));
end
smooth_data=[cpc_data(:,1:7) s_data];
clear s_data
save('-hdf5',[work_dir 'cpc_data.h5'],'cpc_headers','cpc_data','smooth_data');
figure(1);
plot(smooth_data(:,1),smooth_data(:,8),'.k',smooth_data(:,1),smooth_data(:,9),'.b');
ylim([0 12000]);
legend('CPC3007','CPC3772');
datetick();
title('1min running average smoothed data');
figure(2);
plot(cpc_data(:,1),cpc_data(:,8),'.k',cpc_data(:,1),cpc_data(:,9),'.b');
ylim([0 12000]);
legend('CPC3007','CPC3772');
datetick();
title('1s running average smoothed data');