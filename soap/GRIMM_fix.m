clear all
# fclose all
# close all
# Read the pasted GRIMM file
page_screen_output(0);
page_output_immediately(1);
# Set the scene
work_dir='/home/gustavo/data/didactic/FixedSites/UniversityRoof/';
work_file='grimm_data.txt';
fixed_file=[work_file(1:end-4) '_fix.txt'];
id_in=fopen([work_dir work_file],'rt');
id_ou=fopen([work_dir fixed_file],'w');
# Write headers
first_line={'OctaveDate','Year','Month','Day','Hour','Minute','Error', ...
  '265', ...
'290', ...
'324', ...
'374', ...
'424', ...
'474', ...
'539', ...
'614', ...
'675', ...
'748', ...
'894', ...
'1140', ...
'1442', ...
'1789', ...
'2236', ...
'2739', ...
'3240', ...
'3742', ...
'4472', ...
'5701', ...
'6982', ...
'7984', ...
'9220', ...
'11180', ...
'13693', ...
'16202', ...
'18708', ...
'22361', ...
'27386', ...
'30984', ...
'35000'};
dp=[265 290 324 374 424 474 539 614 675 748 894 1140  1442  1789  2236  2739  3240  3742  4472  5701  6982  7984  9220  11180 13693 16202 18708 22361 27386 30984 35000];
dlogDp=[0.0492180227  0.0299632234  0.0669467896  0.057991947 0.0511525224  0.0457574906  0.0644579892  0.0494853631  0.0321846834  0.057991947 0.096910013 0.1139433523  0.0901766303  0.096910013 0.096910013 0.079181246 0.0669467896  0.057991947 0.096910013 0.1139433523  0.0621479067  0.0543576623  0.0705810743  0.096910013 0.079181246 0.0669467896  0.057991947 0.096910013 0.079181246 0.0280287236  1];
fmt_hdr='%s';
for i=1:36
  fmt_hdr=[fmt_hdr '\t%s'];
end
fmt_hdr=[fmt_hdr '\t%s\n'];
fmt_data='%f\t%d\t%d\t%d\t%d\t%d\t%d';
for i=1:30
  fmt_data=[fmt_data '\t%f'];
end
fmt_data=[fmt_data '\t%f\n'];
pad_data=NaN*ones(1,31);
fprintf(id_ou,fmt_hdr,first_line{:});
# Read the first lines until encountering a P line
c_line=fgetl(id_in);
while and(~feof(id_in),c_line(1)!='P')
  c_line=fgetl(id_in);
  c_line
end
while ~feof(id_in)
  #The starting point is a P line that's already in c_line
  #Get date, time and error code
  p_vec=sscanf(c_line(4:end),'%f')';
  octave_date=datenum([p_vec(1:5) 0]);
  data_vec1=[octave_date p_vec(1:5) p_vec(8)];
  #Get the number concentration lines (C records)for the whole minute
  next_P=(0==1);
  invalid=0;
  data_vec2=zeros(1,31);
  for imin=1:1
    #get 4 lines of data and populate the data vector
    #1
    if ~feof(id_in)
      c_line=fgetl(id_in);
    else
      invalid=1;
      break;
    end
    if and(c_line(1)=='C',length(c_line)>73)
      data_vec2(1:8)=data_vec2(1:8)+0.1*sscanf(c_line(4:74),'%f')';
    else
      invalid=1;
      break;
    end
    #2
    if ~feof(id_in)
      c_line=fgetl(id_in);
    else
      invalid=1;
      break;
    end
    if and(c_line(1)=='C',length(c_line)>73)
      #This line has the repeated 2500nm channel
      data_vec2(9:15)=data_vec2(9:15)+0.1*sscanf(c_line(4:65),'%f')';
    else
      invalid=1;
      break;
    end
    #3
    if ~feof(id_in)
      c_line=fgetl(id_in);
    else
      invalid=1;
      break;
    end
    if and(c_line(1)=='c',length(c_line)>73)
      data_vec2(16:23)=data_vec2(16:23)+0.1*sscanf(c_line(4:74),'%f')';
    else
      invalid=1;
      break;
    end
    #4
    if ~feof(id_in)
      c_line=fgetl(id_in);
    else
      invalid=1;
      break;
    end
    if and(c_line(1)=='c',length(c_line)>73)
      data_vec2(24:31)=data_vec2(24:31)+0.1*sscanf(c_line(4:74),'%f')';
    else
      invalid=1;
      break;
    end
  end
  if invalid
    #There were not 10x4 lines of data, i.e., not a full minute
    #So, print an invalid record
    fprintf(id_ou,fmt_data,[data_vec1 pad_data]);
  else
    #We have a full minute so print the record to file
    #First, we calculate the dN/dlogDp for each channel,
    #except the last one that keeps as is
    data_vec2(1:30)=data_vec2(1:30)-data_vec2(2:31);
    data_vec2=data_vec2./dlogDp;
    fprintf(id_ou,fmt_data,[data_vec1 data_vec2]);
  end
  #Now look for the next P line
  next_P=0;
  while and(~feof(id_in),~next_P)
    c_line=fgetl(id_in);
    if length(c_line)>75
      next_P=c_line(1)=='P';
    else
      next_P==0;
    end
  end
end
fclose(id_in);
fclose(id_ou);


# Pad a pasted file according to the timestep selected
fclose all
clear
page_screen_output(0);
page_output_immediately(1);
# Set the scene
work_dir='/home/gustavo/data/didactic/FixedSites/UniversityRoof/';
work_file='grimm_data_fix.txt';
fixed_file=[work_file(1:end-8) '_full.txt'];
id_in=fopen([work_dir work_file],'rt');
id_ou=fopen([work_dir fixed_file],'w');
# pad record ... no timestamp
pad_vec=NaN*ones(1,32);
fmt_data='%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d';
for i=1:30
  fmt_data=[fmt_data '\t%f'];
end
fmt_data=[fmt_data '\t%f\n'];
# Timestep expected ... in vector form [Yr Mo Dy Hr Mi Se]
timestep=[0 0 0 0 1 0];
# Write headers
t_line=fgetl(id_in);
fprintf(id_ou,'%s\n',t_line);
# Read first line and set starting date
t_line=fgetl(id_in);
data_vec=sscanf(t_line,'%f')';
fprintf(id_ou,fmt_data,[data_vec(1:6) 0 data_vec(7:end)]);
curr_date_num=datenum([data_vec(2:6) 0]+timestep);
datestr(curr_date_num)
next_report=curr_date_num+1;
while ~feof(id_in)
  #read next data line
  t_line=fgetl(id_in);
  data_vec=sscanf(t_line,'%f')';
  #get DATE to which this record belongs
  next_date_num=datenum([data_vec(2:6) 0]);
  #Compare the record_date with the expected date. If the record date is into the future,
  #output a PAD line to the file and increase expected date value.
  while next_date_num>curr_date_num
    fprintf(id_ou,fmt_data,[curr_date_num datevec(curr_date_num) pad_vec]);
    curr_date_num=curr_date_num+datenum(timestep);
    'missing data'
  end
  fprintf(id_ou,fmt_data,[curr_date_num datevec(curr_date_num) data_vec(7:end)]);
  curr_date_num=curr_date_num+datenum(timestep);
  if curr_date_num>next_report
    datestr(curr_date_num)
    next_report=next_report+1;
  end
end
fclose(id_in);
fclose(id_ou);
#Now load the relevant data and save the HDF5 file
grimm_data=dlmread([work_dir fixed_file],'\t',1,0);
grimm_data(find(grimm_data(:,8)!=0),9:end)=NaN;
grimm_headers={'OctaveDate','Year','Month','Day','Hour','Second', 'Minute','Error', ...
'265', ...
'290', ...
'324', ...
'374', ...
'424', ...
'474', ...
'539', ...
'614', ...
'675', ...
'748', ...
'894', ...
'1140', ...
'1442', ...
'1789', ...
'2236', ...
'2739', ...
'3240', ...
'3742', ...
'4472', ...
'5701', ...
'6982', ...
'7984', ...
'9220', ...
'11180', ...
'13693', ...
'16202', ...
'18708', ...
'22361', ...
'27386', ...
'30984', ...
'35000'};
grimm_dp=[265 290 324 374 424 474 539 614 675 748 894 1140  1442  1789  2236  2739  3240  3742  4472  5701  6982  7984  9220  11180 13693 16202 18708 22361 27386 30984 35000];
grimm_dlogDp=[0.0492180227  0.0299632234  0.0669467896  0.057991947 0.0511525224  0.0457574906  0.0644579892  0.0494853631  0.0321846834  0.057991947 0.096910013 0.1139433523  0.0901766303  0.096910013 0.096910013 0.079181246 0.0669467896  0.057991947 0.096910013 0.1139433523  0.0621479067  0.0543576623  0.0705810743  0.096910013 0.079181246 0.0669467896  0.057991947 0.096910013 0.079181246 0.0280287236  1];
save('-hdf5',[work_dir 'grimm_data.h5'],'grimm_headers','grimm_dp','grimm_dlogDp','grimm_data');
figure(1);
plot((grimm_data(1:end-1,1)-grimm_data(2:end,1))*24*60);




