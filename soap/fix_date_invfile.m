%clean problems with NaN date records
fclose all
close all
clear
page_screen_output(0);
page_output_immediately(1);
# Set the scene
work_dir='/home/olivaresga/DATA/SOAP/smps_data/';
work_file='inv_smps_full.txt';
fixed_file='inv2_smps_full.txt';
id_in=fopen([work_dir work_file],'rt');
id_ou=fopen([work_dir fixed_file],'w');
lin=fgetl(id_in);
fprintf(id_ou,'%s\n',lin);
while ~feof(id_in)
  lin=fgetl(id_in);
  if lin(1)~='N'
    fprintf(id_ou,'%s\n',lin);
  end
end
fclose(id_in);
fclose(id_ou);