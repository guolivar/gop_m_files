fclose all
close all
# Read the pasted aethalometer file (daily) 1:15 behind in the clock
# Set the scene
page_screen_output(0);
page_output_immediately(1);
work_dir='/home/gustavo/data/soap/ae21_data/';
work_file='ae21.txt';
fixed_file=[work_file(1:end-4) '_fix.txt'];
id_in=fopen([work_dir work_file],'rt');
id_ou=fopen([work_dir fixed_file],'w');
# Write headers
# Expanded Data Format:
# 1	Date
# 2	Time
# 3	1	BC
# 4	2	uvBC
# 5	3	Flow
# 6	4	bypass
# 7	5	BCzero
# 8	6	BCsense
# 9	7	BCref0
# 10	8	BCrefbeam
# 11	9	BCatt
# 12	10	uvBCzero
# 13	11	uvBCsense
# 14	12	uvBCref0
# 15	13	uvBCrefbeam
# 16	14	uvBCatt
first_line={'OctaveDate','Year','Month','Day','Hour','Minute','BC','uvBC','Flow','BC_att','UV_att','Date'};
fprintf(id_ou,'%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n',first_line{:});
while ~feof(id_in)
  t_line=fgetl(id_in);
  commas=findstr(t_line,',');
  DateText=t_line(1:commas(2)-1);
  DateText(DateText==',')=' ';
  DateText(DateText=='"')='';
  DateVec=datevec([DateText ':00'],0)+[2000 0 0 0 0 0];
  data_vec=sscanf(t_line(commas(2)+1:end),'%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f');
  data_out=[datenum(DateVec) DateVec(1:end-1) data_vec([1 2 3 9 15])'];
  fprintf(id_ou,'%f\t%d\t%d\t%d\t%d\t%d\t%f\t%f\t%f\t%f\t%f\t',data_out);
  fprintf(id_ou,'%s\n',datestr(data_out(1),"'yyyy-mm-dd HH:MM:SS'"))
end
fclose(id_in);
fclose(id_ou);
