#fclose all
#close all
# Read the pasted CPC file (daily)
page_screen_output(0);
page_output_immediately(1);
# Set the scene
work_dir='/home/olivaresga/DATA/SOAP/cpc_data/';
work_file='cpc_raw_data.txt';
fixed_file=[work_file(1:end-4) '_fix.txt'];
id_in=fopen([work_dir work_file],'rt');
id_ou=fopen([work_dir fixed_file],'w');
# Write headers
first_line={'OctaveDate','Year','Month','Day','Hour','Minute','Second','CPC3007','CPC3772','WCPC3781'};
fprintf(id_ou,'%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n',first_line{:});
#format string
fmt_rd='';
for i=1:23
	fmt_rd=[fmt_rd '%f\t'];
end
fmt_rd=[fmt_rd '%f'];
imworking=0;
notice=imworking+5000;
    strict_fails=0;
    fails=0;
while ~feof(id_in)
  imworking=imworking+1;
  if imworking>=notice
    disp(['I am still at it, be patient ... ' num2str(notice)]);
    notice=notice+5000;
  end
  t_line=fgetl(id_in);
  #Is it a headers line?
  if t_line(1)~='Y'
    data_vec=sscanf(t_line,fmt_rd);
    #Data QA
    #3007
    if data_vec(7)>0
      cpc3007=data_vec(7);
    else
      cpc3007=NaN;
    end
    #3772
    #parse error bit code
    err_tm=dec2bin(hex2dec(num2str(data_vec(17))));
    err_3772=fliplr(err_tm);
    for i=length(err_3772)+1:16;
      err_3772=[err_3772 '0'];
    end
    #critical errors are: Flow Rate (4 and 5) *Concentration(8) Liquid Level(7)
    if or(data_vec(8)==0,err_3772(8)=='1',data_vec(8)>10000,err_3772(4)=='1',err_3772(5)=='1',err_3772(7)=='1')
		#count the strict fails
# 		strict_fails=strict_fails+1;
# 	end
#     #relax error conditions
#     if or(err_3772(4)=='1',err_3772(5)=='1')
		fails=fails+1;
      cpc3772=NaN;
    else
      cpc3772=data_vec(8);
    end
    #3781 Data 3781 [19_3781flag 20_3781cnt 21_3781mode 22_3781LivTime 23_3781SamTime 24_3781Photo]
    if ~isnan(data_vec(18))
      #Check the flags
      err_w3781=data_vec(19)>0;
      #Live time?
      err_w3781=err_w3781|(data_vec(22)==0);
      #Non-zero concentrations?
      err_w3781=err_w3781|(data_vec(18)<=0);
      if ~err_w3781
        wcpc3781=data_vec(18);
      else
        wcpc3781=NaN;
      end
    else
      wcpc3781=NaN;
    end
    data_out=[datenum(data_vec(1:6)') data_vec(1:6)' cpc3007 cpc3772 wcpc3781];
    fprintf(id_ou,'%f\t%d\t%d\t%d\t%d\t%d\t%d\t%f\t%f\t%f\n',data_out);
  end
end
fclose(id_in);
fclose(id_ou);
strict_fails
fails
cpc_data=dlmread([work_dir fixed_file],'\t',1,0);