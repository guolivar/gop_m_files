page_screen_output(0);
page_output_immediately(1);
work_dir='/home/gustavo/data/soap/cpc_data/';
cpc=load([work_dir 'cpc_data.h5']);
flags_3772='cpc3772_flags.txt';
flags_3772_data=dlmread([work_dir flags_3772],'\t',1,0);
notes_flag=ones(size(cpc.cpc_data(:,1:2)));

nBevents=length(flags_3772_data(:,1));
for i=1:nBevents
  date1=datenum(flags_3772_data(i,1:6));
  date2=datenum(flags_3772_data(i,7:12));
  inx=find(and(cpc.cpc_data(:,1)>=date1,cpc.cpc_data(:,1)<=date2));
  length(inx)
  notes_flag(inx,1)=0;
end

flags_3007='cpc3007_flags.txt';
flags_3007_data=dlmread([work_dir flags_3007],'\t',1,0);
nBevents=length(flags_3007_data(:,1));
for i=1:nBevents
  date1=datenum(flags_3007_data(i,1:6));
  date2=datenum(flags_3007_data(i,7:12));
  inx=find(and(cpc.cpc_data(:,1)>=date1,cpc.cpc_data(:,1)<=date2));
  length(inx)
  notes_flag(inx,2)=0;
end

