# Pad a pasted file according to the timestep selected
fclose all
close all
# Read the pasted CPC file (daily)
page_screen_output(0);
page_output_immediately(1);
# Set the scene
work_dir='/run/media/olivaresga/Win7Portable/soap_data/ae21_data/';
work_file='aethalometer_fix.txt';
fixed_file=[work_file(1:end-8) '_full.txt'];
id_in=fopen([work_dir work_file],'rt');
id_ou=fopen([work_dir fixed_file],'w');
# Format for data read ... and write
fmt='%f\t%d\t%d\t%d\t%d\t%d\t%f\t%f\t%f\t%f\t%f';
# pad record ... no timestamp
pad_vec=[NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN];
# Timestep expected ... in vector form [Yr Mo Dy Hr Mi Se]
timestep=[0 0 0 0 5 0];
# Write headers
t_line=fgetl(id_in);
fprintf(id_ou,'%s\n',t_line);
# Read first line and set starting date
t_line=fgetl(id_in);
data_vec=sscanf(t_line,fmt)';
curr_date_num=datenum([data_vec(2:6) 0]+timestep);
fprintf(id_ou,'%s\n',t_line);
#while ~feof(id_in)
for ij=1:5
  t_line=fgetl(id_in);
  data_vec=sscanf(t_line,fmt)';
  next_date_num=datenum([data_vec(2:6) 0]);
  datevec(abs(next_date_num-curr_date_num)+1)
#   while next_date_num>curr_date_num
#     fprintf(id_ou,[fmt '\n'],[curr_date_num pad_vec]);
#     curr_d_vec=curr_d_vec+timestep
#     curr_date_num=datenum(curr_d_vec)
#     'missing data'
#     pause
#   end
  fprintf(id_ou,'%s\n',t_line);
  curr_d_vec=datevec(datenum(curr_d_vec+timestep))
  next_date_num=datenum(curr_d_vec)
  pause
end
fclose(id_in);
fclose(id_ou);