# Pad a pasted file according to the timestep selected
fclose all
# close all
# Read the pasted CPC file (daily)
page_screen_output(0);
page_output_immediately(1);
# Set the scene
work_dir='/home/gustavo/data/soap/ae21_data/';
work_file='ae21_fix.txt';
fixed_file=[work_file(1:end-8) '_full.txt'];
id_in=fopen([work_dir work_file],'rt');
id_ou=fopen([work_dir fixed_file],'w');
# Format for data read ... and write
fmt='%f\t%d\t%d\t%d\t%d\t%d\t%d\t%f\t%f\t%f\t%f\t%f';
# pad record ... no timestamp
pad_vec=[NaN NaN NaN NaN NaN];
# Timestep expected ... in vector form [Yr Mo Dy Hr Mi Se]
timestep=[0 0 0 0 5 0];
# Write headers
t_line=fgetl(id_in);
fprintf(id_ou,'%s\n',t_line);
# Read first line and set starting date
t_line=fgetl(id_in);
data_vec=sscanf(t_line,'%f')';
curr_date_num=datenum([data_vec(2:6) 0]+timestep);
fprintf(id_ou,'%s\n',t_line);
while ~feof(id_in)
  #read next data line
  t_line=fgetl(id_in);
  data_vec=sscanf(t_line,'%f')';
  #get DATE to which this record belongs
  next_date_num=datenum([data_vec(2:6) 0]);
  #Compare the record_date with the expected date. If the record date is into the future,
  #output a PAD line to the file and increase expected date value.
  while next_date_num>curr_date_num
    fprintf(id_ou,[fmt '\n'],[curr_date_num datevec(curr_date_num) pad_vec]);
    curr_date_num=curr_date_num+datenum(timestep);
    'missing data'
  end
  fprintf(id_ou,[fmt '\n'],[curr_date_num datevec(curr_date_num) data_vec(7:end)]);
  curr_date_num=curr_date_num+datenum(timestep);
end
fclose(id_in);
fclose(id_ou);
aethalometer_data=dlmread([work_dir fixed_file],'\t',1,0);
aethalometer_headers={'OctaveDate','Year','Month','Day','Hour','Minute','Second','BC','uvBC','Flow','BC_att','UV_att'};
save('-hdf5',[work_dir 'aethalometer_data.h5'],'aethalometer_headers','aethalometer_data');