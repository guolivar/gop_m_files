pkg load instrument-control
page_screen_output(0);
page_output_immediately(1); 
# Opens serial port ttyUSB1 with baudrate (config defaults to 8-N-1)
s1 = serial("/dev/ttyUSB1", 57600) 
Tstart=now;
Tend=Tstart+24/24; #24 hr of logging
inarray=zeros(1,100);
nsecs2plot=36000;	# Number of seconds in the plot window
data_plot=NaN*zeros(nsecs2plot,15);
data_plot_prev=data_plot;
plot([0 1],[0 0]);
# Flush input and output buffers
srl_flush(s1);
i=0;
while now<Tend
  i=i+1;
  inbyte=srl_read(s1,1);
  ii=1;
  while inbyte!=10 # EoL = 10 ('\n')
    inarray(ii)=inbyte;
    ii=ii+1;
    inbyte=srl_read(s1,1);
  end
  data_str=char(inarray(1:ii-1));
  inarray=zeros(1,100);
  data_vec=sscanf(data_str,'%f');
#   data_vec=[1 datevec(now) 1000*rand(1,8)];
#   data_vec(14)=data_vec(14)>500;
  OK_nelements=length(data_vec)==15;
  OK_RTC=data_vec(2)>2010;
  if and(OK_nelements,OK_RTC)
    data_plot=[data_plot_prev(2:end,:);data_vec'];
    data_plot_prev=data_plot;
  #   date_plot=datenum(data_plot(:,2:7))	;
    date_plot=(1:nsecs2plot);
    CO= [0; and(data_plot(2:end,end)==1,data_plot(1:end-1,end)==2)];
    # LAYOUT:	[Dist] [Temp]
    # 		[CO2 ] [CO  ]
    # 		[   Dust    ]
    if i==5
      subplot(3,2,1);
      h1=plot(date_plot,data_plot(:,8),'-b',date_plot,data_plot(:,14)*100,'*r');
      title('Distance')
      subplot(3,2,2);
      h2=plot(date_plot,data_plot(:,10),'-b',date_plot,data_plot(:,9),'-r');
      legend('T_{in}','T_{out}');
      title('Temperature')
      subplot(3,2,3)
      h3=plot(date_plot,-1*(data_plot(:,12)-2000),'-b',date_plot,(-1+data_plot(:,14))*(1000),'*r');
      title('CO_2');
      subplot(3,2,4)
      h4=plot(date_plot,data_plot(:,13).*CO,'*b',date_plot,data_plot(:,14)*400,'*r');
      title('CO');
      subplot(3,1,3);
      h5=plot(date_plot,data_plot(:,11),'-b',date_plot,data_plot(:,14)*1500,'*r');
      title('PM');
      pause(0.01);
    elseif i>5
      set(h1(1),'ydata',data_plot(:,8));
      set(h1(2),'ydata',data_plot(:,14)*100);
      set(h2(1),'ydata',data_plot(:,10));
      set(h2(2),'ydata',data_plot(:,9));
      set(h3(1),'ydata',-1*(data_plot(:,12)-2000));
      set(h3(2),'ydata',(-1+data_plot(:,14))*(1000));
      set(h4(1),'ydata',data_plot(:,13).*CO);
      set(h4(2),'ydata',data_plot(:,14)*400);
      set(h5(1),'ydata',data_plot(:,11));
      set(h5(2),'ydata',data_plot(:,14)*1500);
      pause(0.01);
    end
  end
end
srl_close(s1);