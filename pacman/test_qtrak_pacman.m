page_screen_output(0);
page_output_immediately(1);
# fclose all
close all
# clear
###############################################
###############   PLOT   ######################
###############################################
work_dir='/home/olivaresga/data/PACMAN_DATA/firmware53/with_qtrak/';
#Output file name ... in the same working directory
ou_file='pacman11_20130618.txt';
###############################################
###############   PLOT   ######################
###############################################
data_full=dlmread([work_dir ou_file],'\t',1,2);
data_full(:,end)=[and(data_full(1:end-1,16)==2,data_full(2:end,16)==1); 0];
# Scale back the data to meaningful units
# Distance
Distance_Scale=(5000/512)/2.54; # mv / cm
data_full(:,9)=(5000/1024)*(1/50)*data_full(:,9)/Distance_Scale;
# Temperature OUT
Temperature_Out_Scale=10; # mv / C
data_full(:,10)=(5000/1024)*(1/50)*data_full(:,10)/Temperature_Out_Scale;
# Temperature IN
data_full(:,11)=data_full(:,11)/100;
# CO2
data_full(:,13)=(5000/1024)*(1/50)*data_full(:,13);

#  Fitting from datasheet and assumed linear amplifier
#  CO2 = a * exp(b*mVsensor)
#  Fitting target of lowest sum of squared absolute error = 2.3600707275671848E+04
a = 25380814677.9385;
b = -0.0558408743;
co2_test=a*exp(b*data_full(:,13)/10.0352);

# CO
data_full(:,14)=(5000/1024)*(1/50)*data_full(:,14);
# Dust
data_full(:,9:14)=data_full(:,9:14);
ou_file_clean=ou_file(1:end-3);
###############################################
############   QTRAK DATA   ###################
###############################################
data_qtrak=dlmread('/home/olivaresga/data/PACMAN_DATA/firmware53/with_qtrak/qtrak_data.txt','\t',1,0);
qtrak_date=datenum(data_qtrak(:,[3 2 1 4 5 6]));

# Temperature inside
indx=11;
figure(1);
plot(data_full(:,1),data_full(:,indx),'-b',qtrak_date,data_qtrak(:,8)+273.15,'-r');
legend('PACMAN','Q-Trak');
title('temperature');
datetick();
saveas(1,[work_dir 'Tin_' ou_file_clean 'png'],'png');

# Carbon Dioxide
indx=13;
figure(2);
plot(data_full(:,1),co2_test,'.b',qtrak_date,data_qtrak(:,7),'.r');
legend('PACMAN','Q-Trak');
title('CO_2');
datetick();
saveas(2,[work_dir 'CO2_' ou_file_clean 'png'],'png');
ylim([350 700]);

# Carbon Monoxide
indx=14;
figure(3);
valid_CO=find(data_full(:,end)==1);
plot(data_full(valid_CO,1),data_full(valid_CO,indx),'.b',qtrak_date,data_qtrak(:,10)*1000,'.r');
legend('PACMAN','Q-Trak');
title('CO');
datetick();
saveas(3,[work_dir 'CO_' ou_file_clean 'png'],'png');

