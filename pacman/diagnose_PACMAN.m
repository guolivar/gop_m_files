# plot 1 pacman file for diagnostic purposes
page_screen_output(0);
page_output_immediately(1);
%% Which file to work with
work_dir='./';
work_file='KARA_full.txt';
data_full=dlmread(['./' work_file],'\t',1,0);
# Moving average window length [seconds]
# Plot the different fields
# Distance
indx=9;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('Distance');
datetick();
saveas(1,['Distance_' work_file '.png'],'png');
close(1)

# Temperature outside
indx=10;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('External temperature');
datetick();
saveas(1,[work_dir 'Tout_' work_file '.png'],'png');
close(1)

# Temperature inside
indx=11;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('Internal temperature');
datetick();
saveas(1,[work_dir 'Tin_' work_file '.png'],'png');
close(1)

# Dust
indx=12;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('Dust');
datetick();
saveas(1,[work_dir 'Dust_' work_file '.png'],'png');
close(1)

# Carbon Dioxide
indx=13;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('CO_2');
datetick();
saveas(1,[work_dir 'CO2_' work_file '.png'],'png');
close(1)

# Carbon Monoxide
indx=14;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('CO');
datetick();
saveas(1,[work_dir 'CO_' work_file '.png'],'png');
close(1)

# Movement
indx=15;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('Movement');
datetick();
saveas(1,[work_dir 'Movement_' work_file '.png'],'png');
close(1)
