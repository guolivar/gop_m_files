%Save data to HDF5 format

%read column headers
data_path='/home/gustavo/data/pacman/HB2012_full.txt';
id_in=fopen(data_path,'r');
linea=fgetl(id_in);
fclose(id_in);
breaks=findstr("\t",linea);
j=1;
for i=1:length(breaks)
  headers{i}=linea(j:breaks(i)-1);
  j=breaks(i)+1;
end
headers{length(breaks)+1}=linea(j:end);
data=dlmread(data_path,'\t',1,0);
description='Healthy Buildings 2012 conference test cell. Green wall tests in particle generating environment';
author='Guy Coulson';
save -hdf5 hb2012.h5 description author headers data
