# Workfile to deal with CO_2 scaling and source ID
work_dir=['/home/gustavo/data/pacman/PIF/13/'];
work_file='R_201307_clean.txt';
data=dlmread([work_dir work_file],'\t',1,0);
# Expected median concentration to be ~450 ppm which is equivalent to 319.5 mV
# data_full(:,13)=data_full(:,14)*319.5/medCO2;
dust=data(:,12);
co=data(:,14);
co(find(data(:,17)==0))=NaN;
co2_raw=data(:,13);
# co2_corr=co2_raw*319.5;
# Conversion from mV to ppm according to:
# EXP((mV-428.5291254278)/-17.8429640112)
co2=2./co2_corr;
plot(co2);
ylim([0 1000]);
