# plot 2 pacman file for comparison purposes
page_screen_output(0);
page_output_immediately(1);
%% Which file to work with
work_dir1='/home/gustavo/data/pacman/BritishColumbia/tests/PACMAN14/';
work_file1='pacman14_test.txt';
work_dir2='/home/gustavo/data/pacman/BritishColumbia/tests/PACMAN10/';
work_file2='pacman10_test.txt';
data1=dlmread([work_dir1 work_file1],'\t',1,0);
data2=dlmread([work_dir2 work_file2],'\t',1,0);
legend1='Outdoor';
legend2='Indoor';
# Moving average window length [seconds]
window=60;
# Plot 4 fields: Temperature, CO2, Dust, Movement
figure(1);
# Temperature
indx=11;
subplot(3,1,1);
plot(data1(:,1),filter(1/window*ones(1,window),1,data1(:,indx)),'-r', ...
  data2(:,1),filter(1/window*ones(1,window),1,data2(:,indx)),'-b', ...
  data2(:,1),filter(1/window*ones(1,window),1,data2(:,15))*30,'-k');
legend(legend1,legend2,'Movement');
title('Internal temperature');
datetick();
ylim([0 35]);

# Dust
indx=12;
subplot(3,1,2);
plot(data1(:,1),filter(1/window*ones(1,window),1,data1(:,indx)),'-r', ...
  data2(:,1),filter(1/window*ones(1,window),1,data2(:,indx)),'-b', ...
  data2(:,1),filter(1/window*ones(1,window),1,data2(:,15))*150+100,'-k');
title('Dust');
datetick();
ylim([100 250]);

# Carbon Dioxide
indx=13;
subplot(3,1,3);
plot(data1(:,1),filter(1/window*ones(1,window),1,data1(:,indx)),'-r', ...
  data2(:,1),filter(1/window*ones(1,window),1,data2(:,indx)),'-b', ...
  data2(:,1),filter(1/window*ones(1,window),1,data2(:,15))*1000+2000,'-k');
title('CO_2');
datetick();
ylim([2000 3000]);

saveas(1,'/home/olivaresga/DATA/PACMAN_DATA/BritishColumbia/comparison_14_14_Feb09_16.png');
close(1);
