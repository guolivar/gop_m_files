# plot 2 pacman file for comparison purposes
page_screen_output(0);
page_output_immediately(1);
fclose all
close all
clear
%% Which file to work with
work_dir1='/home/gustavo/data/pacman/BritishColumbia/telkwa/PACMAN14_SUB18_Feb09-Feb162013_TelkwaBC/';
work_file1='pacman14_full.txt';
work_dir2='/home/gustavo/data/pacman/BritishColumbia/telkwa/PACMAN10_SUB18_Feb09-Feb16_2013_TelkwaBC/';
work_file2='pacman10_full.txt';
ydata1=dlmread([work_dir1 work_file1],'\t',1,0);
xdata2=dlmread([work_dir2 work_file2],'\t',1,0);
ydata2=xdata2(:,3:19);
ydata1(:,end)=[and(ydata1(1:end-1,16)==2,ydata1(2:end,16)==1); 0];
co_1=find(ydata1(:,end)==1);
ydata2(:,end)=[and(ydata2(1:end-1,16)==2,ydata2(2:end,16)==1); 0];
co_2=find(ydata2(:,end)==1);
data1=ydata1(co_1,:);
data2=ydata2(co_2,:);
legend1='Outdoor';
legend2='Indoor';
# Moving average window length [seconds]
window=60;
# Plot 3 ratios and Temperature: CO2/CO, Dust/CO, Dust/CO2
figure(1);
# CO/CO2
indx1=13; #CO2
indx2=14; #CO
subplot(4,1,1);
plot(data1(:,1),(data1(:,indx1)./data1(:,indx2)),'-r', ...
  data2(:,1),(data2(:,indx1)./data2(:,indx2)),'-b', ...
  ydata2(:,1),filter(1/window*ones(1,window),1,ydata2(:,15))*100,'-k');
legend(legend1,legend2,'Movement');
title('CO_2/CO');
datetick();
# ylim([0 35]);

# Dust/CO
indx1=12; #Dust
indx2=14; #CO
subplot(4,1,2);
plot(data1(:,1),(data1(:,indx1)./data1(:,indx2)),'-r', ...
  data2(:,1),(data2(:,indx1)./data2(:,indx2)),'-b', ...
  ydata2(:,1),filter(1/window*ones(1,window),1,ydata2(:,15))*10,'-k');
title('Dust/CO');
datetick();
# ylim([100 250]);

# Dust/CO2
indx1=12; #Dust
indx2=13; #CO2
subplot(4,1,3);
plot(ydata1(:,1),filter(1/window*ones(1,window),1,ydata1(:,indx1)./ydata1(:,indx2)),'-r', ...
  ydata2(:,1),filter(1/window*ones(1,window),1,ydata2(:,indx1)./ydata2(:,indx2)),'-b', ...
  ydata2(:,1),filter(1/window*ones(1,window),1,ydata2(:,15))*0.1,'-k');
title('Dust/CO_2');
datetick();
# ylim([2000 3000]);

# Temperature
indx=11; #Temperature
subplot(4,1,4);
plot(ydata1(:,1),filter(1/window*ones(1,window),1,ydata1(:,indx)),'-r', ...
  ydata2(:,1),filter(1/window*ones(1,window),1,ydata2(:,indx)),'-b', ...
  ydata2(:,1),filter(1/window*ones(1,window),1,ydata2(:,15))*100,'-k');
title('Temperature');
datetick();
# ylim([2000 3000]);

saveas(1,'/home/gustavo/data/pacman/BritishColumbia/telkwa/comparison_14_14_Feb09_16.png');
# close(1);




# Dust/CO2
indx1=12; #Dust
indx2=13; #CO2
figure(2);
plot(ydata1(:,1),filter(1/window*ones(1,window),1,ydata1(:,indx1).*ydata1(:,indx2)),'-r', ...
  ydata2(:,1),filter(1/window*ones(1,window),1,ydata2(:,indx1).*ydata2(:,indx2)),'-b', ...
  ydata2(:,1),filter(1/window*ones(1,window),1,ydata2(:,15))*0.1,'-k');
title('Dust/CO_2');
datetick();
ylim([0 0.15]);