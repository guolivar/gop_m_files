%	vec2tim_struct Takes in a vector representing a date
%	Only the first 6 elements of the vector represent valid
%	date indicators.
%	If the TimeZone is omitted, the TimeZone of the local machine
%	will be used.
%
%	This function is in the public domain. Gustavo Olivares.
function tm_strc=vec2tim_struct(vec,TimeZone)

nelements=min(length(vec),6);
if nelements=6
	d_vec=vec;
else
	d_vec=[vec zeros(1,6-nelements)];
end
tm_strc_now=gmtime(now())
tm_strc=tm_strc_now
Dstring=datestr(d_vec,0)
[tm_strc,nch]=strptime(Dstring,'%d-%b-%Y %H:%M:%S');
switch nargin()
case 1
%Only time vector given
	tm_strc.zone=tm_strc_now.zone;
case 2
%Both time and TimeZone given
	tm_strc.zone=TimeZone;
otherwise
	tm_strc=gmtime(0);
	return;
endswitch
end
