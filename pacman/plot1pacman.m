# plot 1 pacman file for diagnostic purposes
page_screen_output(0);
page_output_immediately(1);
%% Which file to work with
work_dir='/home/olivaresga/mnt/auckland/groups/AQData/Pacman_5/expt900/pacman10/';
work_file='CO_test_full.txt';
data_full=dlmread([work_dir work_file],'\t',1,0);
# Moving average window length [seconds]
window=60;
# Plot the different fields
# Distance
indx=9;
figure(1);
plot(data_full(:,1),filter(1/window*ones(1,window),1,data_full(:,indx)));
title('Distance');
datetick();

# Temperature outside
indx=10;
figure(2);
plot(data_full(:,1),filter(1/window*ones(1,window),1,data_full(:,indx)));
title('External temperature');
datetick();

# Temperature inside
indx=11;
figure(3);
plot(data_full(:,1),filter(1/window*ones(1,window),1,data_full(:,indx)));
title('Internal temperature');
datetick();

# Dust
indx=12;
figure(4);
plot(data_full(:,1),filter(1/window*ones(1,window),1,data_full(:,indx)));
title('Dust');
datetick();

# Carbon Dioxide
indx=13;
figure(5);
plot(data_full(:,1),filter(1/window*ones(1,window),1,data_full(:,indx)));
title('CO_2');
datetick();

# Carbon Monoxide
indx=14;
figure(6);
plot(data_full(:,1),filter(1/window*ones(1,window),1,data_full(:,indx)));
title('CO');
datetick();

# Movement
indx=15;
figure(7);
plot(data_full(:,1),filter(1/window*ones(1,window),1,data_full(:,indx)));
title('Movement');
datetick();
