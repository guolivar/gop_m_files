# plot 1 pacman file for diagnostic purposes
page_screen_output(0);
page_output_immediately(1);
%% Which file to work with
work_dir='/home/olivaresga/data/PACMAN_DATA/PIF/Amor_Hirao/Outdoor/';
start_file=2013710;
end_file=2013713;
out_file=[num2str(start_file,'%08d') '_full.txt'];
fid_ou=fopen([work_dir out_file],'wt');
for c_file=start_file:end_file
  fid_in=fopen([work_dir num2str(c_file,'%07d') '.TXT']);
  if fid_in
    while ~(feof(fid_in))
      strin=fgetl(fid_in);
      if strin(1)~='D'
	# Replace TABS, / and : for SPACES (char_32)
	idx=strfind(strin,'/');
	strin(idx)=32;
	idx=strfind(strin,':');
	strin(idx)=32;
	idx=strfind(strin,char(9));
	strin(idx)=32;
	fprintf(fid_ou,'%s\n',strin)
      end
    end
  end
end
all_data=dlmread([work_dir out_file],' ');
timestamp=datenum(all_data(:,[1 2 3 7 8 9]));
# Plotting
figure(1);
subplot(3,1,1);
plot(timestamp,all_data(:,16));
title('Temperature');
datetick();
subplot(3,1,2);
plot(timestamp,all_data(:,14));
title('Relative Humidity');
datetick();
subplot(3,1,3);
plot(timestamp,all_data(:,10));
title('Dust');
datetick();