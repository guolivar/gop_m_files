%% Read PACMAN file and plot
page_screen_output(0);
page_output_immediately(1);
%% Which folder to work in
work_dir='/home/gustavo/data/pacman/PIF/';
# Output image filename suffix
image_name='Hirao_Home';
# Input files indices to work with
nfiles=4;
datafile={'R_201307_clean_Bedroom.txt', ...
'R_201307_clean_Heater.txt', ...
'R_201307_clean_Kitchen.txt', ...
'R_201307_clean_Lounge.txt'};
location={'Bedroom','Heater','Kitchen','Lounge'};
plot_format={'-k','-r','-b','-g'};
# Read datafiles
for i=1:nfiles
  data.(location{i})=dlmread([work_dir datafile{i}]);
end
# Plot data
# Temperature (11)
x_title='Temperature';
x_indx=11;
figure(1);
hold on;
for i=1:nfiles
  plot(data.(location{i})(:,1),data.(location{i})(:,x_indx),plot_format{i});
end
title(x_title);
legend(location);
hold off;
datetick();
saveas(1,[work_dir x_title '_' image_name '.png'],'png');
close(1)

# Distance (9)
x_title='Distance';
x_indx=9;
figure(1);
hold on;
for i=1:nfiles
  plot(data.(location{i})(:,1),data.(location{i})(:,x_indx),plot_format{i});
end
title(x_title);
legend(location);
hold off;
datetick();
saveas(1,[work_dir x_title '_' image_name '.png'],'png');
close(1)

# Dust (12)
x_title='Dust';
x_indx=12;
figure(1);
hold on;
for i=1:nfiles
  plot(data.(location{i})(:,1),data.(location{i})(:,x_indx),plot_format{i});
end
title(x_title);
legend(location);
hold off;
datetick();
saveas(1,[work_dir x_title '_' image_name '.png'],'png');
close(1)

# CO2 (13)
x_title='CO_2';
x_indx=13;
figure(1);
hold on;
for i=1:nfiles
  plot(data.(location{i})(:,1),data.(location{i})(:,x_indx),plot_format{i});
end
title(x_title);
legend(location);
hold off;
datetick();
saveas(1,[work_dir x_title '_' image_name '.png'],'png');
close(1)

# Movement (15)
x_title='Movement';
x_indx=15;
figure(1);
hold on;
for i=1:nfiles
  plot(data.(location{i})(:,1),data.(location{i})(:,x_indx),plot_format{i});
end
title(x_title);
legend(location);
hold off;
datetick();
saveas(1,[work_dir x_title '_' image_name '.png'],'png');
close(1)

# CO (14)
x_title='CO';
x_indx=14;
figure(1);
hold on;
for i=1:nfiles
  valid_indx=find(data.(location{i})(:,17)==1);
  plot(data.(location{i})(valid_indx,1),data.(location{i})(valid_indx,x_indx),plot_format{i});
end
title(x_title);
legend(location);
hold off;
datetick();
saveas(1,[work_dir x_title '_' image_name '.png'],'png');
close(1)
