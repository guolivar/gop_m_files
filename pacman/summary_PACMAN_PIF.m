# 	summary_PACMAN_PIF performs a simple analysis and summarizes
# 	the data in a PACMAN output file generating a CLEAN_PACMAN output file
# 	and a set of plots as PNG images documenting the process
# 	
# 	raw_file_path =	FULL path for the folder where the RAW file(s) are [STRING]
# 	start_Fname =	Start DATE for the files to be processed (YYYYMMDD) [INTEGER]
# 	end_Fname =	End DATE for the files to be processed (YYYYMMDD)
# 			Needs to be EQUAL OR LARGER than start_Fname [INTEGER]
# 	ou_suffix =	TEXT added to the output filenames to identify specific experiments.
# 	out_file_path =	FULL path for the OUTPUT file. Note that the images
# 			will be saved in the same path and with similar names. [STRING]
#
# 	This function is in the public domain. Gustavo Olivares.
function status=summary_PACMAN_PIF(raw_file_path,start_Fname,end_Fname,ou_suffix,out_file_path)
%% Read PACMAN file and plot
page_screen_output(0);
page_output_immediately(1);
%% Which file to work with
work_dir=raw_file_path;
#File indices to work with
start_idx=start_Fname
end_idx=end_Fname
#Output file name ... in the same working directory
out_file_path
ou_file=[num2str(start_idx,'%d') '_' ou_suffix '.txt']
fid_ou=fopen([out_file_path ou_file],'w');
headers={'Date_octave', ...
'Count', ...
'Year', ...
'Month', ...
'Day', ...
'Hour', ...
'Minute', ...
'Second', ...
'Distance', ...
'Temperature_mV', ...
'Temperature_IN_C', ...
'PM_mV', ...
'CO2_mV', ...
'CO_mV', ...
'Movement', ...
'COstatus', ...
'COvalid'};
fmt_hrd='';
for xi=1:16
  fmt_hrd=[fmt_hrd '%s\t'];
end
fmt_hrd=[fmt_hrd '%s\n'];
fprintf(fid_ou,fmt_hrd,headers{:})
#Vector to pad the data output when invalid records are found
pad_vec=[NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN];
d_vec=zeros(1,15);
first_date=1;
COstatus_prev=2;
CO=0;
for f_idx=start_idx:end_idx
  in_file=[num2str(f_idx,'%d') '.TXT'];
  printf('\n\n%s\n',in_file);
  [nfo err1 msg1]=stat([work_dir in_file]);
  #Check that the file actually has some data in ... more than a couple of lines!
  if nfo.size>1000
    #Open file and process
    fid_in=fopen([work_dir in_file],'r');
    while ~feof(fid_in)
      strin=fgetl(fid_in);
      if length(strin)<10
	strin(1)='C';
      end
      if strin(1)~='C'
        while (1==1)
          p_d_vec=d_vec;
          d_vec=sscanf(strin,'%f')';
          %establish error condition
          %Number of elements in the record must be 15 
          OK_nelements=length(d_vec)==15;
          %Valid date
          if OK_nelements
            % (catching error 165 from the RTC) ... date above 1999
            OK_RTC=d_vec(2)>1010;
            %repeated records
            if first_date
              first_date=0;
              OK_same_record=1;
              curr_date_num=datenum(d_vec(2:7));
              curr_d_vec=d_vec(2:7);
            else
              OK_same_record=~all(p_d_vec(2:7)==d_vec(2:7));
            end
            if ~OK_same_record
              printf('%s\t%s\n','Repeated',datestr(datenum(d_vec(2:7)),0));
            end
          else
	    d_vec=p_d_vec;
	  end
          if and(OK_nelements,OK_RTC,OK_same_record)
            break;
          else
            strin=fgetl(fid_in);
          end        
        end
        COstatus_curr=d_vec(end);
        if and(COstatus_curr==1,COstatus_prev==2)
	  CO=1;
        else
	  CO=0;
        end
        next_date_num=datenum(d_vec(2:7));
        while next_date_num>curr_date_num
	  printf('%s\t%s\n','GAP ',datestr(datenum(d_vec(2:7)),0));
          fprintf(fid_ou,'%.8f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n',[curr_date_num pad_vec]);
          curr_d_vec=curr_d_vec+[0 0 0 0 0 1];
          curr_date_num=datenum(curr_d_vec);
          CO=0;
        end
        fprintf(fid_ou,'%.8f\t%s\t%f\n',curr_date_num,strin,CO);
        COstatus_prev=COstatus_curr;
        curr_d_vec=curr_d_vec+[0 0 0 0 0 1];
        curr_date_num=datenum(curr_d_vec);
      end
    end
    fclose(fid_in);
  end
end
fclose(fid_ou);
###############################################
###############   PLOT   ######################
###############################################
data_full=dlmread([work_dir ou_file],'\t',1,0);
data_full(:,end)=[and(data_full(1:end-1,16)==2,data_full(2:end,16)==1); 0];
%# Scale the concentrations to their MEDIAN
%#Dust
data_full(:,12)=data_full(:,12)/nanmedian(data_full(:,12));
%#Carbon Monoxide
data_full(:,14)=data_full(:,14)/nanmedian(data_full(:,14));
%#Carbon Dioxide
medCO2=nanmedian(data_full(:,13));
%# Expected median concentration to be ~450 ppm which is equivalent to 319.5 mV
# data_full(:,13)=data_full(:,14)*319.5/medCO2;
data_full(:,13)=data_full(:,13)/medCO2;
dlmwrite([work_dir 'R_' ou_file],data_full,'\t');
ou_file_clean=ou_file(1:end-3);
# Plot the different fields
# Distance
indx=9;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('Distance');
datetick();
saveas(1,[work_dir 'Distance_' ou_file_clean 'png'],'png');
close(1)

# Temperature outside
indx=10;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('External temperature');
datetick();
saveas(1,[work_dir 'Tout_' ou_file_clean 'png'],'png');
close(1)

# Temperature inside
indx=11;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('Internal temperature');
datetick();
saveas(1,[work_dir 'Tin_' ou_file_clean 'png'],'png');
close(1)

# Dust
indx=12;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('Dust');
datetick();
saveas(1,[work_dir 'Dust_' ou_file_clean 'png'],'png');
close(1)

# Carbon Dioxide
indx=13;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('CO_2');
datetick();
saveas(1,[work_dir 'CO2_' ou_file_clean 'png'],'png');
close(1)

# Carbon Monoxide
indx=14;
figure(1);
valid_CO=find(data_full(:,end)==1);
plot(data_full(valid_CO,1),data_full(valid_CO,indx));
title('CO');
datetick();
saveas(1,[work_dir 'CO_' ou_file_clean 'png'],'png');
close(1)

# Movement
indx=15;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('Movement');
datetick();
saveas(1,[work_dir 'Movement_' ou_file_clean 'png'],'png');
close(1)



# Distance
indx=9;
figure(1);
subplot(4,2,1);
plot(data_full(:,1),data_full(:,indx));
title('Distance');
datetick();

# Movement
indx=15;
subplot(4,2,2);
plot(data_full(:,1),data_full(:,indx));
title('Movement');
datetick();

# Temperature outside
indx=10;
subplot(4,2,3);
plot(data_full(:,1),data_full(:,indx));
title('External temperature');
datetick();

# Temperature inside
indx=11;
subplot(4,2,4);
plot(data_full(:,1),data_full(:,indx));
title('Internal temperature');
datetick();

# Carbon Dioxide
indx=13;
subplot(4,2,5);
plot(data_full(:,1),data_full(:,indx));
title('CO_2');
datetick();

# Carbon Monoxide
indx=14;
subplot(4,2,6);
valid_CO=find(data_full(:,end)==1);
plot(data_full(valid_CO,1),data_full(valid_CO,indx));
title('CO');
datetick();


# Dust
indx=12;
subplot(4,1,4);
plot(data_full(:,1),data_full(:,indx));
title('Dust');
datetick();
saveas(1,[work_dir 'diagnostic_' ou_file_clean 'png'],'png');
close(1)



