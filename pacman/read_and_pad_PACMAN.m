%% Read PACMAN file and plot
page_screen_output(0);
page_output_immediately(1);
fclose all
close all
clear
%% Which file to work with
work_dir='/home/olivaresga/data/PACMAN_DATA/firmware53/atMktLane/pacman12/';
#File indices to work with
start_idx=4;
end_idx=11;
#Output file name ... in the same working directory
ou_file='pacman12_MktPlace.txt';
fid_ou=fopen([work_dir ou_file],'w');
headers={'Date', ...
'Time', ...
'Date_octave', ...
'Count', ...
'Year', ...
'Month', ...
'Day', ...
'Hour', ...
'Minute', ...
'Second', ...
'Distance', ...
'Temperature_mV', ...
'Temperature_IN_C', ...
'PM_mV', ...
'CO2_mV', ...
'CO_mV', ...
'Movement', ...
'COstatus', ...
'COvalid'};
fmt_hrd='';
for xi=1:18
  fmt_hrd=[fmt_hrd '%s\t'];
end
fmt_hrd=[fmt_hrd '%s\n'];
fprintf(fid_ou,fmt_hrd,headers{:})
#Vector to pad the data output when invalid records are found
pad_vec=[NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN NaN];
d_vec=zeros(1,15);
first_date=1;
COstatus_prev=2;
CO=0;
curr_date_num=now();
curr_d_str=[datestr(curr_date_num,'yyyy/mm/dd%tHH:MM:SS')];
for f_idx=start_idx:end_idx
  in_file=['PAM_' num2str(f_idx,'%d') '.TXT'];
  printf('\n\n%s\n',in_file);
  [nfo err1 msg1]=stat([work_dir in_file]);
  #Check that the file actually has some data in ... more than a couple of lines!
  if nfo.size>1000
    #Open file and process
    fid_in=fopen([work_dir in_file],'r');
    while ~feof(fid_in)
      strin=fgetl(fid_in);
      if strin(1)~='C'
#         while (1==1)
          p_d_vec=d_vec;
          d_vec=sscanf(strin,'%f')';
#           %establish error condition
#           %Number of elements in the record must be 15 
#           OK_nelements=length(d_vec)==15;
#           %Valid date
#           if OK_nelements
#             % (catching error 165 from the RTC) ... date above 1999
#             OK_RTC=d_vec(2)>2010;
#             %repeated records
#             if first_date
#               first_date=0;
#               OK_same_record=1;
#               curr_date_num=datenum(d_vec(2:7));
#               curr_d_vec=d_vec(2:7);
#               curr_d_str=[datestr(curr_date_num,'yyyy/mm/dd%tHH:MM:SS')];
#             else
#               OK_same_record=~all(p_d_vec(2:7)==d_vec(2:7));
#             end
#             if ~OK_same_record
#               printf('%s\t%s\n','Repeated',datestr(datenum(d_vec(2:7)),0));
#             end
#           else
# 	    d_vec=p_d_vec;
# 	  end
#           if and(OK_nelements,OK_RTC,OK_same_record)
#             break;
#           else
#             strin=fgetl(fid_in);
#           end        
#         end
        COstatus_curr=d_vec(end);
        if and(COstatus_curr==1,COstatus_prev==2)
	  CO=1;
        else
	  CO=0;
        end
        next_date_num=datenum(d_vec(2:7));
#         while next_date_num>curr_date_num
# 	  printf('%s\t%s\n','GAP ',datestr(datenum(d_vec(2:7)),0));
# 	  fprintf(fid_ou,'%s\t%.8f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n',curr_d_str,[curr_date_num pad_vec]);
#           curr_d_vec=curr_d_vec+[0 0 0 0 0 1];
#           curr_date_num=datenum(curr_d_vec);
#           curr_d_str=[datestr(curr_date_num,'yyyy/mm/dd%tHH:MM:SS')];
#           CO=0;
#         end
        fprintf(fid_ou,'%s\t%.8f\t%s\t%f\n',curr_d_str,curr_date_num,strin,CO);
        COstatus_prev=COstatus_curr;
#         curr_d_vec=curr_d_vec+[0 0 0 0 0 1];
#         curr_date_num=datenum(curr_d_vec);
#         curr_d_str=[datestr(curr_date_num,'yyyy/mm/dd%tHH:MM:SS')];
      end
    end
    fclose(fid_in);
  end
end
fclose(fid_ou);
###############################################
###############   PLOT   ######################
###############################################
data_full=dlmread([work_dir ou_file],'\t',1,2);
data_full(:,end)=[and(data_full(1:end-1,16)==2,data_full(2:end,16)==1); 0];
ou_file_clean=ou_file(1:end-3);
data_full(:,1)=(1:length(data_full(:,1)));
# Plot the different fields
# Distance
indx=9;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('Distance');
# datetick();
saveas(1,[work_dir 'Distance_' ou_file_clean 'png'],'png');
close(1)

# Temperature outside
indx=10;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('External temperature');
# datetick();
saveas(1,[work_dir 'Tout_' ou_file_clean 'png'],'png');
close(1)

# Temperature inside
indx=11;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('Internal temperature');
# datetick();
saveas(1,[work_dir 'Tin_' ou_file_clean 'png'],'png');
close(1)

# Dust
indx=12;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('Dust');
# datetick();
saveas(1,[work_dir 'Dust_' ou_file_clean 'png'],'png');
close(1)

# Carbon Dioxide
indx=13;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('CO_2');
# datetick();
saveas(1,[work_dir 'CO2_' ou_file_clean 'png'],'png');
close(1)

# Carbon Monoxide
indx=14;
figure(1);
valid_CO=find(data_full(:,end)==1);
plot(data_full(valid_CO,1),data_full(valid_CO,indx));
title('CO');
# datetick();
saveas(1,[work_dir 'CO_' ou_file_clean 'png'],'png');
close(1)

# Movement
indx=15;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('Movement');
# datetick();
saveas(1,[work_dir 'Movement_' ou_file_clean 'png'],'png');
close(1)

#  Fitting from datasheet and assumed linear amplifier
#  CO2 = a * exp(b*mVsensor)
#  Fitting target of lowest sum of squared absolute error = 2.3600707275671848E+04
a = 25380814677.9385;
b = -0.0558408743;
co2_test=a*exp(b*data_full(:,13)/10.0352);
# Carbon Dioxide
figure(1);
plot(data_full(:,1),co2_test);
title('CO_2 a*exp(b*data_full(:,13)/10.0352)');
# datetick();
saveas(1,[work_dir 'CO2_CORR_' ou_file_clean 'png'],'png');
close(1)
