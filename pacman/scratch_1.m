###############################################
###############   PLOT   ######################
###############################################
data_full=dlmread([work_dir ou_file],'\t',1,2);
data_full(:,end)=[and(data_full(1:end-1,16)==2,data_full(2:end,16)==1); 0];
%# Scale the concentrations to their MEDIAN
%#Dust
data_full(:,12)=data_full(:,12)/nanmedian(data_full(:,12));
%#Carbon Monoxide
data_full(:,14)=data_full(:,14)/nanmedian(data_full(:,14));
%# Remove first 2hr of data (~7000 records)
data_full(1:7000,14)=NaN;
%#Carbon Dioxide
medCO2=nanmedian(data_full(:,13));
%# Expected median concentration to be ~450 ppm which is equivalent to 319.5 mV
co2_mv=data_full(:,13)*319.5/medCO2;
%# Conversion from mV to ppm according to:
%# EXP((mV-428.5291254278)/-17.8429640112)
data_full(:,13)=exp((co2_mv-428.5291254278)/-17.8429640112);
%# Remove first 2hr of data (~7000 records)
data_full(1:7000,13)=NaN;
%# Renormalize to the median for CO2 concentrations
data_full(:,13)=data_full(:,13)/nanmedian(data_full(:,13));
%# Save to new R file
dlmwrite([work_dir 'R_' ou_file],data_full,'\t');
ou_file_clean=ou_file(1:end-3);
# Plot the different fields
# Distance
indx=9;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('Distance');
datetick();
saveas(1,[work_dir 'Distance_' ou_file_clean 'png'],'png');
close(1)

# Temperature outside
indx=10;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('External temperature');
datetick();
saveas(1,[work_dir 'Tout_' ou_file_clean 'png'],'png');
close(1)

# Temperature inside
indx=11;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('Internal temperature');
datetick();
saveas(1,[work_dir 'Tin_' ou_file_clean 'png'],'png');
close(1)

# Dust
indx=12;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('Dust');
datetick();
saveas(1,[work_dir 'Dust_' ou_file_clean 'png'],'png');
close(1)

# Carbon Dioxide
indx=13;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('CO_2');
datetick();
saveas(1,[work_dir 'CO2_' ou_file_clean 'png'],'png');
close(1)

# Carbon Monoxide
indx=14;
figure(1);
valid_CO=find(data_full(:,end)==1);
plot(data_full(valid_CO,1),data_full(valid_CO,indx));
title('CO');
datetick();
saveas(1,[work_dir 'CO_' ou_file_clean 'png'],'png');
close(1)

# Movement
indx=15;
figure(1);
plot(data_full(:,1),data_full(:,indx));
title('Movement');
datetick();
saveas(1,[work_dir 'Movement_' ou_file_clean 'png'],'png');
close(1)
