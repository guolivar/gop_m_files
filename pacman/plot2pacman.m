# plot 2 pacman file for comparison purposes
page_screen_output(0);
page_output_immediately(1);
%% Which file to work with
work_dir1='/home/olivaresga/data/PACMAN_DATA/BritishColumbia/Smithers/PACMAN14-outdoors_SUB31_April01_09_2013/';
work_file1='pacman14_outdoor_Apr01_09_2013.txt';
work_dir2='/home/olivaresga/data/PACMAN_DATA/BritishColumbia/Smithers/PACMAN10_SUB31_April01_09_2013/';
work_file2='pacman10_20130331.txt';
plot_dir='/home/olivaresga/data/PACMAN_DATA/BritishColumbia/Smithers/';
data1=dlmread([work_dir1 work_file1],'\t',1,2);
data2=dlmread([work_dir2 work_file2],'\t',1,2);
data1(:,end)=[and(data1(1:end-1,16)==2,data1(2:end,16)==1); 0];

data2(:,end)=[and(data2(1:end-1,16)==2,data2(2:end,16)==1); 0];

validC0_1=(data1(:,17)==1);
validC0_2=(data2(:,17)==1);
ou_file_clean='Expt900';
# Moving average window length [seconds]
window=60;
# Plot the different fields
# Distance
indx=9;
figure(1);
plot(data1(:,1),filter(1/window*ones(1,window),1,data1(:,indx)),'-r', ...
  data2(:,1),filter(1/window*ones(1,window),1,data2(:,indx)),'-b');
legend(work_file1,work_file2);
title('Distance');
datetick();
saveas(1,[plot_dir 'Distance_' ou_file_clean 'png'],'png');

# Temperature outside
indx=10;
figure(2);
plot(data1(:,1),filter(1/window*ones(1,window),1,data1(:,indx)),'-r', ...
  data2(:,1),filter(1/window*ones(1,window),1,data2(:,indx)),'-b');
legend(work_file1,work_file2);
title('External temperature');
datetick();
saveas(2,[plot_dir 'Tout_' ou_file_clean 'png'],'png');

# Temperature inside
indx=11;
figure(3);
plot(data1(:,1),filter(1/window*ones(1,window),1,data1(:,indx)),'-r', ...
  data2(:,1),filter(1/window*ones(1,window),1,data2(:,indx)),'-b');
legend(work_file1,work_file2);
title('Internal temperature');
datetick();
saveas(3,[plot_dir 'Tin_' ou_file_clean 'png'],'png');

# Dust
indx=12;
figure(4);
plot(data1(:,1),filter(1/window*ones(1,window),1,data1(:,indx)),'-r', ...
  data2(:,1),filter(1/window*ones(1,window),1,data2(:,indx)),'-b');
legend(work_file1,work_file2);
title('Dust');
datetick();
saveas(4,[plot_dir 'Dust_' ou_file_clean 'png'],'png');

# Carbon Dioxide
indx=13;
figure(5);
plot(data1(:,1),filter(1/window*ones(1,window),1,data1(:,indx)),'-r', ...
  data2(:,1),filter(1/window*ones(1,window),1,data2(:,indx)),'-b');
legend(work_file1,work_file2);
title('CO_2');
datetick();
saveas(5,[plot_dir 'CO2_' ou_file_clean 'png'],'png');

# Carbon Monoxide
indx=14;
figure(6);
plot(data1(validC0_1,1),data1(validC0_1,indx),'-r', ...
  data2(validC0_2,1),data2(validC0_2,indx),'-b');
# ylim([0 25])
legend(work_file1(1:8),work_file2(1:8));
title('CO');
datetick();
saveas(6,[plot_dir 'CO_' ou_file_clean 'png'],'png');

# Movement
indx=15;
figure(7);
plot(data1(:,1),filter(1/window*ones(1,window),1,data1(:,indx)),'-r', ...
  data2(:,1),filter(1/window*ones(1,window),1,data2(:,indx)),'-b');
legend(work_file1,work_file2);
title('Movement');
datetick();
saveas(7,[plot_dir 'Movement_' ou_file_clean 'png'],'png');

