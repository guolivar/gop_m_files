# Pad a pasted file according to the timestep selected
# fclose all
# close all
# Read the pasted CPC file (daily)
page_screen_output(0);
page_output_immediately(1);
# Set the scene
work_dir='/home/gustavo/data/inv_mod/';
work_file='data_qst.txt';
temp_file='iterations.txt';
id_temp=fopen([work_dir temp_file],'w');
data=dlmread([work_dir work_file],'\t',1,0);
date_matrix=[datevec(data(:,1)) weekday(data(:,1))];
%wdir_mask=data(:,4)
# x0=[ones(48,1);zeros(12,1)];
x0=rand(48,1)*0;
[xx,xv,nev]=minimize('error_EF_Qst',{x0,date_matrix(:,[1 2 3 4 7]),data(:,2),data(:,3),id_temp});
xx
xv
nev
plot([0:23],xx(1:24),'-k',[0:23],xx(25:48),'-r');
fclose(id_temp);