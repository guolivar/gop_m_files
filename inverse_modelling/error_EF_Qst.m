# Calculate the SE for the given parameter vector and model-observation pairs
# Inputs:
# x	: Parameter vector. Length 60 (weekday
# 	and weekend diurnal variation PLUS monthly varying background)
# Vdate	: Date matrix in YYYY MM DD HH WKD format
# obs	: Observations vector (same lenght as Vdate)
# model	: Model results vector (same length as Vdate)
# fid_temp: File indicator to output intermediate steps
# 
# Outputs:
# SErr	: Sum of the squared errors \sum{(obs-model(EFs))^2}

function	SErr=error_EF_Qst(x,Vdate,obs,model,fid_temp)
workday=max(0,x(1:24));
weekend=max(0,x(25:48));
nobs=length(Vdate);
SErr=0;
for i=1:nobs
  if and(~isnan(model(i)),~isnan(obs(i)))
    if and(Vdate(i,5)>1,Vdate(i,5)<7)
      ef=workday(Vdate(i,4)+1);
    else
      ef=weekend(Vdate(i,4)+1);
    end
    ef=max(0,ef);
    SErr=SErr+(obs(i)-(model(i)*ef))^2;
  else
    SErr=SErr;
  end
end
fmt_out=['%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t'];
fmt_out=[fmt_out fmt_out fmt_out fmt_out '%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n'];
# fmt_out=[fmt_out fmt_out '%f\t%f\t%f\t%f\t%f\n'];
fprintf(fid_temp,fmt_out,[SErr workday' weekend']);
plot([0:23],workday,'-k',[0:23],weekend,'-r');
pause(0.01);
SErr