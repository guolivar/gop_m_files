%experiments for now
page_screen_output(0);
page_output_immediately(1);
work_folder='/home/olivaresga/DATA/marlborough/first_pass/';
file_name='marlborough_full.data';
out_file='marlborough_full_mat.data';
id_in=fopen([work_folder file_name],'rt');
id_ou=fopen([work_folder out_file],'wt');
%for i=1:100
i=1;
while ~feof(id_in)
  clear datavec grimmdata rawgrimm
  lin=fgetl(id_in);
  datavec=sscanf(lin,'%f')';
  if length(datavec)>=100
    rawgrimm=datavec(57:end);
    grimmdata=rawgrimm(find(rawgrimm~=0));
    if length(grimmdata)<30
      grimmdata=NaN*ones(1,30);
    end
    vec_length(i)=56+30;
    fmt_wr='%.6f';
    for j=2:vec_length(i)
      fmt_wr=[fmt_wr '\t%.6f'];
    end
    fmt_wr=[fmt_wr '\n'];
    if or(abs(datavec(25))<41,abs(datavec(25))>42)
      datavec(25)=NaN;
    end
    if or(abs(datavec(26))<173,abs(datavec(26))>175)
      datavec(26)=NaN;
    end
    fprintf(id_ou,fmt_wr,[datavec(1:56) grimmdata(1:30)]);
    i=i+1;
  end
end
fclose(id_in);
fclose(id_ou);