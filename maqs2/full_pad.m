%Generate 1s interval datafile padding where necessary.
page_screen_output(0);
page_output_immediately(1);
work_folder='/home/olivaresga/DATA/marlborough/first_pass/';
data_file='marlborough_full_mat.data';
full_file='all_data.data';
id_in=fopen([work_folder data_file],'rt');
id_full=fopen([work_folder full_file],'w');
c_lin=fgetl(id_in);
fprintf(id_full,'%s\n',['OctaveDate' 9 c_lin]);
fmt_wr='%.6f';
for j=1:86
  fmt_wr=[fmt_wr '\t%.6f'];
end
fmt_wr=[fmt_wr '\n'];
nan_data=NaN*ones(1,86);
curr_date=datenum([2012 8 17 15 47 27]);
while ~feof(id_in)
  #read next data line
  c_lin=fgetl(id_in);
  c_datavec=sscanf(c_lin,'%f')';
  #get DATE to which this record belongs
  next_date=datenum(c_datavec(1,1:6));
  #compare the record date with the expected date. If the record date is into the future,
  #output a PAD line to file and increase expected date value.
  while curr_date<next_date
    %pad an empty line with date
    fprintf(id_full,fmt_wr,[curr_date nan_data]);
    %move the date by 1 second
    curr_date=curr_date+datenum([0 0 0 0 0 1]);
    'missing data'
  end
  fprintf(id_full,fmt_wr,[curr_date c_datavec]);
  %move the date by 1 second
  curr_date=curr_date+datenum([0 0 0 0 0 1]);
end
fclose(id_in);
fclose(id_full);
