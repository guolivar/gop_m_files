%Generate average time series of a set interval.
page_screen_output(0);
page_output_immediately(1);
work_folder='/home/olivaresga/DATA/marlborough/first_pass/';
data_file='all_data.data';
full_file='tseries_10min.data';
timestep=10*60; #In seconds
id_in=fopen([work_folder data_file],'rt');
headers=fgetl(id_in);
fclose(id_in);
data_in=dlmread([work_folder data_file],'\t',1,0);
id_ou=fopen([work_folder full_file],'w');
fprintf(id_ou,'%s\n',headers);
nrecs=length(data_in(:,1));
noutp=floor(nrecs/timestep);
fmt_wr='%.6f';
for j=1:86
  fmt_wr=[fmt_wr '\t%.6f'];
end
fmt_wr=[fmt_wr '\n'];
for i=1:noutp
  xfr=1+(i-1)*timestep;
  xto=i*timestep;
  data_out=nanmean(data_in(xfr:xto,:));
  fprintf(id_ou,fmt_wr,data_out);
end
fclose(id_ou);
