%Group data and pad missing records.
page_screen_output(0);
page_output_immediately(1);
work_folder='/home/olivaresga/DATA/marlborough/first_pass/';
data_file='marlborough_full_mat.data';
full_file='all_data.data';
id_in=fopen([work_folder data_file],'rt');
id_full=fopen([work_folder full_file],'w');
c_lin=fgetl(id_in);
fprintf(id_full,'%s\n',['OctaveDate' 9 c_lin]);
fmt_wr='%.6f';
for j=1:86
  fmt_wr=[fmt_wr '\t%.6f'];
end
fmt_wr=[fmt_wr '\n'];
nan_data=NaN*ones(1,86);
while ~feof(id_in)
  c_lin=fgetl(id_in);
  c_datavec=sscanf(c_lin,'%f')';
  start_date=datenum(c_datavec(1,1:6));
  %Set the output file
  id_ou=fopen([work_folder datestr(start_date,'yyyymmddHH_') data_file],'wt');
  curr_date=start_date;
  %Group UP TO 8 hours of data
  end_date=datenum(datevec(start_date) + [0 0 0 12 0 0]);
  fprintf(id_ou,fmt_wr,[start_date c_datavec]);
  curr_date=datenum(datevec(curr_date)+[0 0 0 0 0 1]);
  while and(curr_date<=end_date,~feof(id_in))
    c_lin=fgetl(id_in);
    c_datavec=sscanf(c_lin,'%f')';
    next_date=datenum(c_datavec(1,1:6));
    while and(curr_date<=end_date,curr_date<next_date)
      %pad an empty line with date
      fprintf(id_ou,fmt_wr,[curr_date nan_data]);
      %move the date by 1 second
      curr_date=datenum(datevec(curr_date)+[0 0 0 0 0 1]);
    end
    if curr_date<=end_date
      %curr_date is next_date
      fprintf(id_ou,fmt_wr,[curr_date c_datavec]);
      fprintf(id_full,fmt_wr,[curr_date c_datavec]);
    end
    %move the date by 1 second
    curr_date=datenum(datevec(curr_date)+[0 0 0 0 0 1]);
  end
  %Finished 8hr package
  fclose(id_ou);
end
fclose(id_in);
fclose(id_full);
