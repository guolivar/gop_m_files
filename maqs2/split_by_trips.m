%experiments for now
close all;
page_screen_output(0);
page_output_immediately(1);
work_folder='/home/olivaresga/DATA/marlborough/first_pass/';
data_file={'2012081715_marlborough_full_mat.data'
'2012081807_marlborough_full_mat.data'
'2012081819_marlborough_full_mat.data'
'2012082216_marlborough_full_mat.data'
'2012082315_marlborough_full_mat.data'
'2012082415_marlborough_full_mat.data'};
id_aux=fopen([work_folder 'Marlborough_Data_headers.txt'],'rt');
headers=fgetl(id_aux);
fclose(id_aux);
clear id_aux;
nfiles=length(data_file);
for file=1:nfiles
  data=dlmread([work_folder data_file{file}],'\t');
  ntrips=max(data(:,8));
  for trip=0:ntrips
    trip_indxs=find(data(:,8)==trip);
    id_out=fopen([work_folder data_file{file}(1:10) '_' num2str(trip) '.data'],'wt');
    fprintf(id_out,'%s\n',headers);
    dlmwrite(id_out,data(trip_indxs,:),'delimiter','\t','precision','%.6f','-append');
    clear trip_indxs;
  end
  clear data;
end